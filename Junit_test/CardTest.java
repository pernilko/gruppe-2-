import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    Card card;

    @Before
    public void setUp(){
        card = new Card (2, 3);
    }

    @Test
    public void getSuit(){
        assertTrue(card.getSuit() == 3);
    }

    @Test
    public void getValue(){
        assertTrue(card.getValue() == 2);
    }

    @Test
    public void toStringTest(){
        assertEquals("Value: 2 Suit: Diamond", card.toString());
    }

    public static void main(String[] args){
        org.junit.runner.JUnitCore.main(CardTest.class.getName());
    }
}