import org.junit.*;
import java.util.ArrayList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class GameTest {

    private static Game game;
    private static ArrayList<Player> players;
    private static ArrayList<Card> cards;

    @AfterClass
    public static void tearDown(){
        Database.disconnect();
    }

    @BeforeClass
    public static void beforeClass(){
        players = new ArrayList<>();
        players.add(new Player("TesterOne", "T1", 0));
        players.add(new Player("TesterTwo", "T2", 1));
        players.add(new Player("TesterThree", "T3", 2));
        players.add(new Player("TesterFour", "T4", 3));

        game = new Game(10, players, 1, 0, 1);
        game.updateGameState();
        game.setTurn(0);

        cards = new ArrayList<>();
        cards.add(new Card(2,3));
        cards.add(new Card(2, 3));
        cards.add(new Card(1,1));
        cards.add(new Card(2,2));

        players.get(0).setHand(cards);
    }

    @After
    public void after(){
        game.setTurn(0);
        players.get(0).setTricksTaken(0);
        int[] highestBidderAndBid = {0,0};
        game.setHighestBidderAndBid(highestBidderAndBid);
        Database.setTeamMember1(1,2);
    }

    @Test
    public void setTrumpTest(){
        Database.setTrump(0, 1);
        game.setTrump();
        assertTrue(game.getTrump() == 3);
    }

    @Test
    public void updateBidsTest(){
        assertTrue(game.getTurn() == 0);
        game.updateBid();
        assertTrue(game.getTurn() == 1);
        game.updateBid();
        assertTrue(game.getTurn() == 2);
        game.updateBid();
        assertTrue(game.getTurn() == 2);
    }

    @Test
    public void updateTeamInfoTest(){
        game.setRequestedCardJUnit(new Card(-1, -1));
        game.setTurn(3);
        assertTrue(game.getTurn() == 3);
        game.updateTeamInfo();
        int[] team = game.getTeam();
        assertTrue(team[0] == 2);
        assertTrue(team[1] == 0);
        assertTrue(game.getTurn() == 0);
    }

    @Test
    public void setRequestedCard(){
        game.setTurn(3);
        boolean res = game.setRequestedCard(new Card(2,2));
        assertFalse(res);
        game.setTurn(0);
        res = game.setRequestedCard(new Card(2, 2));
        assertFalse(res);
    }

    @Test
    public void playCardTest(){
        game.setCurrentSuit(1);
        assertFalse(game.playCard(new Card(2,2)));
        game.setTurn(2);
        assertFalse(game.playCard(new Card(2,2)));
    }

    @Test
    public void placeBidTest(){
        game.setTurn(2);
        assertFalse(game.placebid(3));
        game.setTurn(0);
        assertTrue(game.placebid(-1));
    }

    @Test
    public void updateScoreTest(){
        int[] highestBidderAndBid = {2, 2};
        game.setHighestBidderAndBid(highestBidderAndBid);
        players.get(0).setScore(0);
        players.get(0).addTricksTaken(3);
        game.updateScore();
        System.out.println("yo" + players.get(0).getScore());
        assertTrue(players.get(0).getScore() == 2);
    }

    @Test
    public void getMiniRoundWinnerTest(){
        game.setCurrentSuit(1);
        assertTrue(game.getMiniRoundWinner() == 1);
        game.setTrumpStandard(4);
        assertTrue(game.getMiniRoundWinner() == 2);
    }

    @Test
    public void endOfbiddingRoundHandlerTest(){
        assertTrue(game.getTurn() == 0);
        int[] highestBidderAndBid = {3, 7};
        game.setHighestBidderAndBid(highestBidderAndBid);
        game.endOfbiddingRoundHandler();
        assertTrue(game.getTurn() == 3);
    }


    public static void main(String[] args){
        org.junit.runner.JUnitCore.main(GameTest.class.getName());
    }
}
