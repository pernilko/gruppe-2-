import org.junit.AfterClass;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class DatabaseTest {

    @AfterClass
    public static void tearDown(){
        Database.disconnect();
    }

    @Test
    public void getHashPassword() {
        String expRes = Database.getPasswordHash("maria");
        assertEquals("1000:16F717B389ADCCE6A75F016CE36901A8:FC68196D2F5F751BAC7471947C11CB44687879E4B67A4191714E564DEB92EB42A368FA3F83D54A6A05B9C58B43D26B5A7224AB6BB53049955B6ECE5C256E9F6D",expRes);
    }

    @Test
    public void getPlayersInParty(){
        ArrayList<Player> result = Database.getPlayersInParty(2);
        Player player1 = result.get(0);
        Player player2 = result.get(1);
        Player player3 = result.get(2);

        assertEquals(player1.getUsername(), "maria");
        assertEquals(player2.getUsername(), "pernille");
        assertEquals(player3.getUsername(), "BRbright");
    }

    @Test
    public void getRandomizedCards(){

        int number_of_cards = 4;
        ArrayList<Card> c = Database.getRandomizedCards(1, number_of_cards);

        assertTrue(c.size() == number_of_cards);
    }

    @Test
    public void refreshDeck(){
        Boolean res = Database.refreshDeckOfCards(4);
        assertTrue(res == true);
    }

    @Test
    public void clearPlayedCards(){
        Boolean res = Database.resetTable(3);
        assertTrue(res == true);
    }

    @Test
    public void writePlayedCard(){
        Boolean res = Database.setPlayedCard(new Card(2,2 ), 1, 3);
        assertTrue(res == true);
    }

    @Test
    public void writeBid(){
        boolean res = Database.setBid(200, 3, 3);
        assertTrue(res == true);
    }



    @Test
    public void cleanForBiddingRound(){
        boolean res = Database.cleanForBiddingRound(3);
        assertTrue(res == true);
    }

    @Test
    public void setOtherTeamMate(){
        boolean res = Database.setOtherTeamMate(3, 3);
        assertTrue(res == true);
    }

    @Test
    public void setRequestedCard(){
        boolean res = Database.setRequestedCard(new Card(3, 4), 3);
        assertTrue(res == true);
    }

    @Test
    public void getRequestedCard(){
        boolean resSet = Database.setRequestedCard(new Card(8, 2), 3);
        Card card = Database.getRequestedCard(3);

        assertTrue(card.getValue() == 8);
        assertTrue(card.getSuit() == 2);
    }

    public static void main(String[] args){
        org.junit.runner.JUnitCore.main(DatabaseTest.class.getName());
    }
}