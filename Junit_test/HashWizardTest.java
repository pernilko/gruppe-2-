import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

public class HashWizardTest {

    @After
    public void tearDown(){
        Database.disconnect();
    }

    @Test
    public void generateHash(){
        assertEquals(true, HashWizard.validatePassword("password", HashWizard.generateHash("password")));
        assertEquals(false, HashWizard.validatePassword("password1", HashWizard.generateHash("password")));
    }
}