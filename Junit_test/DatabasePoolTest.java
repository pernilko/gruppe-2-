import org.junit.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.*;


public class DatabasePoolTest {

    @AfterClass
    public static void tearDown(){
        DatabasePool.disconnect();
    }

    @Test
    public void testConnection(){
        Connection connection = DatabasePool.getConnection();
        assertTrue(connection != null);
        DatabasePool.releaseConnection(connection);
    }

    @Test
    public void testConnectionWithSqlsentence(){
        Statement stmt = null;
        ResultSet res = null;
        int expResult = 12;
        int result = 0;
        Connection connection = DatabasePool.getConnection();
        String sqlsentence = "Select games_played from user where username = 'maria'";
        try {
            stmt = connection.createStatement();
            res = stmt.executeQuery(sqlsentence);
            while(res.next()) {
                result = res.getInt("games_played");
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(stmt);
            DatabasePool.releaseConnection(connection);
            assertTrue(result == expResult);
        }
    }

    public static void main(String[] args){
        org.junit.runner.JUnitCore.main(DatabasePoolTest.class.getName());
    }
}