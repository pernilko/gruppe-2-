import org.junit.AfterClass;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.*;

/**
 * Database has to be cleared for the JUnit test to succeed
 * @author team2
 */

public class UserTest {
    User test = new User("random", "randomhei");
    User testJoin = new User("randi", "randipandi");

    @AfterClass
    public static void tearDown(){
        Database.disconnect();
    }

    @Test
    public void createParty() {
        assertEquals("Second Party", test.createParty("Second Party").getPartyName());
        Party active = test.getActiveParty();
        assertEquals("Second Party", active.getPartyName());
    }

    @Test
    public void joinParty() {
        assertEquals(true, testJoin.joinParty(1));
        Party active = testJoin.getActiveParty();
        assertEquals("Test", active.getPartyName());
    }

    @Test
    public void getParties() {
        ArrayList<Party> parties = Admin.getParties();
        for (Party p : parties) {
            assertEquals(p.getPartyName() + ", " + p.getParty_id() + ", " + p.getNumberOfPlayers(), "Second party, 5, 1");
        }
    }
}