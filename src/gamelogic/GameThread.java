/**
 * Creates a thread for every initialized game. Makes it able to run several instances of the same game
 * The thread gets information from the database and updates the state of the game in the game object
 *
 * @author team2
 */
public class GameThread extends Thread {
    private Game game;

    public GameThread(Game game){
        this.game = game;
    }

    public void run(){
        this.setName("GameThread");
        while (!game.isEnded()) {

            if (game.isBiddingRound()) {
                game.updateBid();

            }else if(!game.isTeamCreated()){
                game.updateTeamInfo();
            } else {
                game.updateGameState();
            }
            if (game.getTurn() == -1){
                try {
                    Thread.sleep( 3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                game.endOfMiniRoundHandler();
            }
            game.updateQuitState();

            if(!game.isEnded()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    game.setExit(true);
                }
            }
        }
    }
}
