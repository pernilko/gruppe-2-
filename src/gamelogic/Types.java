/**
 * Defines the different suit types used in the game.
 * @author team2
 */

public class Types {

    public static  final int HEART = 1;
    public static final int CLUBS = 2;
    public static final int DIAMOND = 3;
    public static final int SPADES = 4;
    public static final int TREE = 5;
    public static final int STAR  = 6;


    /**
     * Returns the card suit
     *
     * @param suit the Integer of the requested card suit
     * @return the suit
     */

    public static String getCardSuitString(int suit){

        switch(suit){
            case -1:
                return "Empty Card";

            case 1:
                return "Heart";

            case 2:
                return "Clubs";

            case 3:
                return "Diamond";

            case 4:
                return "Spades";

            case 5:
                return "Tree";

            case 6:
                return "Star";

            case 0:
                return "Empty Card";
            default:
                throw new IllegalArgumentException("Illegal Card Suit");

        }
    }

    /**
     * Returns the Integer value of the suit requested by name
     *
     * @param name the suit name as a String
     * @return the value of the suit.
     */

    public static int getCardValueFromString(String name){
        for(int i =1; i<7; i++){
            if(getCardSuitString(i).equals(name)){
                return i;
            }
        }
        return -1;
    }
}
