import java.util.ArrayList;
import java.util.Collections;

/**
 * Handles all details surrounding a player in game.
 */

public class Player {
    ArrayList<Card> Hand;
    private int tricksTaken;
    private int score;
    private int bid;
    private int playerNumb;
    private String username;
    private String displayname;

    /**
     * Constructs a new player with existing specified username, display name and player number. The player is also
     * handed a hand of cards to play with
     *
     * @param username    the players username
     * @param displayName the players display name
     * @param playerNumb  the players player number
     */
    Player(String username, String displayName, int playerNumb) {
        this.username = username;
        this.displayname = displayName;
        Hand = new ArrayList<>();
        this.playerNumb = playerNumb;

    }

    public String getUsername() {
        return username;
    }

    public String getDisplayname() {
        return displayname;
    }

    public int getTricksTaken() {
        return tricksTaken;
    }

    public int getScore() {
        return score;
    }

    /**
     * Returns a players hand of cards
     *
     * @return the hand of cards
     */
    public ArrayList<Card> getHand() {
        return Hand;
    }

    public int getBid() {
        return bid;
    }

    public int getPlayerNumb() {
        return playerNumb;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public void setScore(int addScore) {
        score = addScore;
    }

    public void addTricksTaken(int tricksTaken) {
        this.tricksTaken += tricksTaken;
    }

    public void setTricksTaken(int tricksTaken) {
        this.tricksTaken = tricksTaken;
    }

    /**
     * Removes the played card from a players hand of cards. Returns the remaining cards
     *
     * @param card the played card
     * @return the remaining cards at hand.
     */
    public Card playCard(Card card) {
        boolean result = Hand.remove(card);

        if (result == true) {
            return card;
        }

        return null;
    }

    /**
     * Fetches cards from the database and sets a hand of cards to a player
     *
     * @param hand the players hand of cards in an array-list
     */
    public void setHand(ArrayList<Card> hand){
        if(hand == null){
            throw new NullPointerException("Can not set hand of value null");
        }
        Card c;

        for(int i = hand.size() -1; i>=0; i-- ){
            c = hand.remove(i);

            this.Hand.add(c);
        }
        sortHand();
    }

    /**
     * Sorts a players hand of cards. The hand is ordered by the card suits.
     *
     */
    private void sortHand(){
        Collections.sort( Hand, (o1, o2) -> {
            if(o1.getSuit() < o2.getSuit()){
                return -1;
            }else if(o1.getSuit() == o2.getSuit()){
                if(o1.getValue() < o2.getValue()){
                    return -1;
                }
                return 1;
            }else{
                return 1;
            }
        } );
    }

    public void addTricksToScore(){
        score += tricksTaken;
        tricksTaken = 0;
    }

    /**
     * Adds to a players score
     *
     * @param add the scores added
     */
    public void addScore(int add){
        score += add;
        tricksTaken = 0;
    }

    /**
     * Clears all the in-game stats; removes cards at hand, number of bids and tricks taken - sets a player up for a
     * new game.
     *
     */
    public void clean(){
        int size = Hand.size();
        for(int i = 0; i < size; i++){
            Hand.remove(0);
        }

        bid = 0;
        tricksTaken = 0;
    }

    /**
     * Returns <code>true</code> if the a player has a requested card in its hand of cards.
     *
     * @param card the requested card
     * @return <code>true</code> if the requested card is there; <code>false</code> otherwise.
     */
    public boolean haveCard(Card card){
        for(Card c : Hand){
            if(c.equals(card)){
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the number of cards in a players hand of cards.
     *
     * @return the number of cards
     */
    public int getNumberOfCards(){
        return Hand.size();
    }

    /**
     * Returns <code>true</code> if the player has the requested current suit in its hand of cards
     *
     * @param currentSuit The suit of the last card played on the table
     * @return <code>true</code> if the player has the requested card; <code>false</code> otherwise.
     */
    public boolean haveCurrentSuit(int currentSuit) {
        for(Card c: Hand){
            if(c.getSuit() == currentSuit){
                return true;
            }
        }
        return false;
    }

    public String toString() {
        int v;
        if (Hand == null) {
            v = 0;
        } else {
            v = Hand.size();
        }
        String s = "TricksTaken: " + tricksTaken + "\nScore: " + score + "\nBid: " + bid + "\nCards: ." + v;
        return s;
    }
}