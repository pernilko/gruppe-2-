import java.util.ArrayList;

/**
 * Controls all the game logic; every action made in the game from start to end.
 *
 * @author team 2
 */
public class Game {

    private ArrayList<Player> players;
    private Card[] cardsOnTable;
    private int round;
    private final int totalRounds;
    private int[] team = new int[2];
    private int startPlayer = 0;
    private int[] highestBidderAndBid = new int[2];
    private boolean exit;
    private boolean isBiddinground;
    private int gameId;
    private int trump;
    private int currentSuit;
    private int playerNumb;
    private int cardsLeft;
    private int playerWithRequestedCard;
    private int turn;
    private int partyId;
    private int turnCounter = 0;// this counter makes sure that turn only increments when a new card has been placed.
    private boolean readyForNewMiniRound;
    private int miniRoundWinner;
    private Thread gameThread;
    private Card requestedCard;

    /**
     * Constructs a new game with its own game ID. Consists of a party with a set of players, identified with a
     * party ID. Each player in game is also handed a player number for User identification.
     *
     * @param totalRounds the total number of rounds in a game.
     * @param players the players in a specific game
     * @param gameId game identification
     * @param playerNumb the Users player number
     * @param partyId party identification; identifies what party the players are part of.
     */
    public Game(int totalRounds, ArrayList<Player> players, int gameId, int playerNumb, int partyId) {
        this.gameId = gameId;
        this.totalRounds = totalRounds;
        this.players = players;
        cardsOnTable = new Card[players.size()];
        exit = false;
        this.playerNumb = playerNumb;
        this.partyId = partyId;
    }

    /**
     * Returns a Users players hand of cards in an array.
     *
     * @return the hand of cards
     */
    public ArrayList<Card> getPlayerHand(){
        return players.get(playerNumb).getHand();
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    /**
     * Returns the number of the round that is currently in play
     *
     * @return the round number.
     */
    public int getRound() { return round; }

    public int getTotalRounds() { return totalRounds; }

    /**
     * Returns the Integer suit value of the cards currently played on the table.
     *
     * @return the suit value
     */
    public int getCurrentSuit() { return currentSuit; }

    public int getGameId(){
        return gameId;
    }

    public int getTrump() { return trump; }

    public int[] getTeam() { return team; }

    /**
     * Returns an array with the played cards on the table
     *
     * @return the cards on the table
     */
    public Card[] getTable() { return cardsOnTable; }

    public int getPlayerNumb() { return playerNumb; }

    /**
     * Returns <code>true</code> if a player got "the European"; maximum tricks of 13 or 16. A player must be in a team
     * for this to be possible.
     *
     * @return <code>true</code> if european is reached; <code>false</code> otherwise.
     */
    public boolean getEuropean() {
        int maxCards = 0;
        int sumTricks = 0;

        if (players.size() == 3) {
            maxCards = 16;
        }else {
            maxCards = 13;
        }

        for (int i = 0; i < players.size(); i++) {
            if ((playerNumb == team[0] || playerNumb == team[1]) && players.get(playerNumb).getBid() == maxCards) {
                sumTricks = players.get(team[0]).getTricksTaken();
                sumTricks += players.get(team[1]).getTricksTaken();
                if (sumTricks == maxCards) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns an array list of all the winners of the game.
     *
     * @return an array list with all the winners
     */
    public ArrayList<Player> getWinner(){
        int score = 0;

        ArrayList<Player> winningPlayers = new ArrayList<>();
        for(Player player : players){
            if(player.getScore() >score){
                score = player.getScore();
            }
        }

        for(Player player : players){
            if(player.getScore() == score){
                winningPlayers.add(player);
            }
        }

        return winningPlayers;
    }

    /**
     * Returns the number of cards left on hand.
     *
     * @return the number of cards
     */
    public int getCardsLeft() { return cardsLeft; }

    public int getTurn() {
        return turn;
    }

    /**
     * Returns the player number of the winner card in each round based on what is locally stored in the cardsOnTable
     * array. The number is aligned to the card object so that the winner of the player number is found from which
     * player placed what card. The method is also used to find the variables trump and currentSuit.
     *
     * @return the player number of the player that won the mini round.
     */
    public int getMiniRoundWinner() {

        int winningCardIndex = 0;
        int cardValue;

        for(int i = 0; i < cardsOnTable.length;i++){
            cardValue = cardsOnTable[winningCardIndex].compareValue(trump,currentSuit,cardsOnTable[i]);
            if(cardValue < 1){
                winningCardIndex = i;
            }
        }
        return winningCardIndex;
    }

    public void setRequestedCardJUnit(Card card){
        requestedCard = card;
    }

    /**
     * Returns <code>true</code> if the requested card is successfully written to the database
     * after checking if the player is allowed to request the card. A card is only requested by the winner of the
     * bidding round.
     *
     * @param card the card that the player requested during the selection of the teams
     * @return <code>true</code> if it is the right players turn and that player has the requested card; <code>false
     * </code> otherwise.
     */
    public boolean setRequestedCard(Card card) {
        if (getTurn() != playerNumb || players.get(playerNumb).haveCard(card)) {

            return false;
        }
        Database.setRequestedCard(card, gameId);
        return true;
    }

    /**
     * The method gets the requested card and sets the trump equal to the suit of that card.
     * Writes the information to the database and updates it locally as well.
     *
     */
    public void setTrump(){
        Card c = Database.getRequestedCard(gameId);
        int suit = c.getSuit();
        Database.setTrump(suit, gameId);

        trump = suit;
    }

    public void setExit(boolean value){
        exit = value;
    }

    public void setTurn(int turn){
        this.turn = turn;
    }

    public void setCurrentSuit(int currentSuit){
        this.currentSuit = currentSuit;
    }

    /**
     * Sets the highest bidders player number and their current bid
     *
     * @param highestBidderAndBid The place at index 0 holds the player number and the index at 1 holds the players bid
     */
    public void setHighestBidderAndBid(int[] highestBidderAndBid){
        if(highestBidderAndBid.length != 2){
            throw new IllegalArgumentException("Illegal table length of the parameter");
        }
        this.highestBidderAndBid = highestBidderAndBid;
    }

    /**
     * Sets the suit of the current card to the suit of the trump
     *
     * @param trump the suit to be set as trump
     */
    public void setTrumpStandard(int trump){
        this.trump = trump;
    }

    /**
     * Returns <code>true</code> if there is an ongoing bidding round.
     *
     * @return <code>true</code> if there is a bidding round; <code>false</code> otherwise.
     */
    public boolean isBiddingRound(){
        return isBiddinground;
    }

    /**
     * Returns <code>true</code> if a team is successfully created.
     *
     * @return <code>true</code> if a team is successfully created; <code>false</code> otherwise.
     */
    public boolean isTeamCreated() {
        if (team[1] == -1) {
            return false;
        }
        return true;
    }

    public boolean isEnded(){
        return exit;
    }

    /**
     * Returns <code>true</code> if it is a players turn. Decided by {@link Game#getTurn()} method.
     *
     * @return <code>true</code> if it is the players turn; <code>false</code> otherwise.
     */
    public boolean myTurn(){
        if(getTurn() == playerNumb){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method is called upon when a game is started. Starts a new thread for the user and pulls information from
     * the database.
     *
     */
    public void startGame(){

        prepareForBiddingRound();

        gameThread = new GameThread(this);
        gameThread.start();
    }

    /**
     * Returns <code>true</code> if current bids is successfully retrieved from the database.
     * Updates the highest bidder in the highestbidderAndBid variable.
     * Updates the turn based upon the bids.
     * If one player has bid and all others has forfeited,
     * the bidding round is over and the method {@link Game#endOfbiddingRoundHandler()} is called
     *
     * @return <code>true</code> if the database a bid; <code>false</code> otherwise.
     */

    public boolean updateBid(){

        // gets bid from database
        int[] newBidsReadFromDatabase = Database.getBids(gameId);

        //returns false if the bids from the database was null
        if(newBidsReadFromDatabase == null || newBidsReadFromDatabase.length< 3){
            return false;
        }

        // updates every players bids with the new bids from the database
        for(int i = 0; i < newBidsReadFromDatabase.length; i++){
            players.get(i).setBid(newBidsReadFromDatabase[i]);
        }

        // loops through the table and finds the highest bidder and sets it in highestBidderAndBid
        for(int i = 0; i < newBidsReadFromDatabase.length; i++){
            if(newBidsReadFromDatabase[i] != 0 && newBidsReadFromDatabase[i] != -1){
                if(newBidsReadFromDatabase[i] > highestBidderAndBid[1]){
                    highestBidderAndBid[0] = i;
                    highestBidderAndBid[1] = newBidsReadFromDatabase[i];
                }
            }
        }

        int notBid = 0;
        int bids = 0;

        // finds how many people has not bid, (notBid) and how many people has bid and not forfeited (bids)
        for(int i = 0; i < newBidsReadFromDatabase.length; i++){
            if(newBidsReadFromDatabase[i] == 0){
                notBid++;
            } else if(newBidsReadFromDatabase[i] != -1){
                bids++;
            }
        }

        // if only one person has a valid bid and everybody else has forfeited, endOfBiddingRoundHandler is triggered.
        if(bids == 1 && notBid == 0){
            endOfbiddingRoundHandler();
            return true;
        }

        // if everybody has forfeited
        if(bids == 0 && notBid == 0){
            endOfbiddingRoundHandlerNobodyBid();
            return true;
        }

        // If current player has forfeited, the turn is passed to the next player
        if(newBidsReadFromDatabase[turn] == -1){
            turn++;
        }

        // if current player has the highest bid, the turn is passed to the next player
        if(turn == highestBidderAndBid[0]){
            turn++;
        }

        // this makes sure the turn loops correct.
        if(turn == players.size()){
            turn = 0;
        }

        return true;
    }

    /**
     * Returns <code>true</code> when the winner of the bidding round requests a card that another player has,
     * and they join together as a team.
     *
     * @return <code>true</code> if a teammate is set or if a card is requested and a player has this card;
     * <code>false</code> otherwise.
     */
    public boolean updateTeamInfo() {

        int[] team = Database.getTeamIndex(gameId);

        if (team != null && team[1] != -1) {

            this.team = team;

            playerWithRequestedCard = team[1];
            miniRoundWinner = playerWithRequestedCard;
            cardsLeft = players.get(playerNumb).getNumberOfCards();

            turn = team[1];

            // update bids of second teammate
            players.get(team[1]).setBid(highestBidderAndBid[1]);
            if(team[1] == playerNumb && requestedCard.isValid()){
                playCard(requestedCard);
            }
            return true;

        } else {
            // if no other teammate, get requested card
            requestedCard = Database.getRequestedCard(gameId);
            if (requestedCard.getSuit() != -1 && players.get(playerNumb).haveCard(requestedCard)) {
                Database.setOtherTeamMate(playerNumb, gameId);
                Database.setTeamMember2(gameId, playerNumb);
                return true;
            }
        }
        return false;
    }

    /**
     * This method gets the state of the table; which card is currently on the table - from the database and
     * updates the game object with information about which player has played what card.
     *
     */
    public void updateGameState(){

        String[][] tableState = Database.getGameState(gameId);

        if(trump == -1){
            setTrump();
        }

        try{
            for (int i = 0; i < cardsOnTable.length; i++) {

                int playerIndex = Integer.parseInt(tableState[i][2]);
                int card_Value = Integer.parseInt(tableState[i][0]);
                int card_Suite = Integer.parseInt(tableState[i][1]);
                cardsOnTable[playerIndex] = new Card(card_Value, card_Suite);

            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        // The game is ready for a new mini round if the database table has been reset to the -1 cards.
        for(Card c: cardsOnTable){
            if(c == null || (c.getSuit() == -1 && c.getValue() == -1)){
                readyForNewMiniRound = true;
            }
        }

        if(readyForNewMiniRound) { // this checks if the players are in a event like "endOfMiniRoundHandler()" that has to be handled before going next.
            updateTurnBasedOnCardsOnTable();
            currentSuit = Database.getCurrentSuit(gameId);
        }
    }

    /**
     * The method updates the turn variable based on how many cards that are on the table. For every card that's been
     * placed on the placed on the on the table, the turn variable increments with one. A variable turnCounter
     * keeps track of how many players that have played a card in a mini round and makes sure that the turn only
     * increments when a new card has been played.
     *
     */
    private void updateTurnBasedOnCardsOnTable(){

        int cardsPlacedOnTable = 0;

        // finds the playernumber of the player that is next to place a card
        for (Card c : cardsOnTable) {
            if (c != null) {
                if (c.getSuit() != -1 && c.getValue() != -1) {
                    cardsPlacedOnTable++;
                }
            }
        }

        //updated the turn only if the turn is not already updated
        if (cardsPlacedOnTable != turnCounter) {
            turn++;
            if (turn == players.size()) {
                turn = 0;
            }
            turnCounter++;
        }

        if (cardsPlacedOnTable == players.size() ) {
            turn = -1;
            turnCounter = 0;
        }
    }

    /**
     * The method updates everyone's tricks to their score both locally and externally to the database.
     * If a player is on a team, both their scores will be added if they reached their bid goal, or
     * subtracted if they did not.
     *
     */
    public void updateScore() {
        // gets the collective tricks taken by the team
        int trickTeamScore = players.get(team[0]).getTricksTaken() + players.get(team[1]).getTricksTaken();

        // adds the tricks to players score
        for (int i = 0; i < players.size(); i++) {

            // if the player with the id i is not on a team, it just adds their tricks to their score
            if (i != team[0] && i != team[1]) {
                players.get(i).addTricksToScore();

            }else { // if the person with the id i is on a team, it adds the collective tricks taken by the team only if they took what they bid ore more);

                if(trickTeamScore < highestBidderAndBid[1]){

                    players.get(i).addScore(-highestBidderAndBid[1]);

                }else {
                    players.get(i).addScore(highestBidderAndBid[1]);
                }
            }
        }

        Database.setScore(players.get(playerNumb).getScore(), playerNumb, gameId);
    }

    /**
     * The method checks if whether someone has quit and left an ongoing game.
     */
    public void updateQuitState() {
        if(!exit) {
            if(Database.gameExists(gameId)) {
                exit = !Database.isGameStarted(partyId);
            }else{
                exit = true;
            }
        }
    }

    /**

     *  The method is called upon if all the players pass in the bidding round. In the event of no bidders, all of
     *  the players are dealt new cards, and then followed up by a new bidding round. Repeated until a player has placed
     *  its bid.
     *
     */
    public void endOfbiddingRoundHandlerNobodyBid(){
        isBiddinground = false;
        players.get(playerNumb).clean();

        // players have to set themselves to ready
        boolean everybodyIsReady = false;

        Database.setPlayerToReady(gameId, playerNumb);

        if(playerNumb == 0) {
            while(!everybodyIsReady){
                if(Database.getReady(gameId)){
                    Database.setAllReadyToFalse(gameId);
                    Database.setEverybodysCurrentBid(gameId);
                    everybodyIsReady = true;
                }
            }
        }
        prepareForBiddingRound();
    }

    /**
     * Method is called when a player wins the bidding round. The method sets the current holder of
     * the highest bid as "first team-mate" and writes it to the database. This player is now the
     * winner of the bidding round and is therefore given the turn to play the first round.
     *
     * the variable isBiddinground set to false
     */
    public void endOfbiddingRoundHandler(){

        team[0] = highestBidderAndBid[0];

        if(playerNumb == team[0]) {
            Database.setTeamMember1(gameId, highestBidderAndBid[0]);
        }

        turn = highestBidderAndBid[0];
        isBiddinground = false;
    }

    /**
     * The method is called on when a mini round has ended. It handles all the statistics by adding the number of tricks
     * of the mini round winner to both database and local storage. Resets cards on table and current suit, and sets
     * the winner as turn starter. Triggers {@link Game#endOfRoundHandler()}.
     *
     */
    public void endOfMiniRoundHandler() {

        readyForNewMiniRound = false;

        miniRoundWinner = getMiniRoundWinner();

        players.get(miniRoundWinner).addTricksTaken(1);
        if (miniRoundWinner == playerNumb) {
            Database.addTrick(gameId, playerNumb);
        }

        // Tells the database that this player is ready
        Database.setPlayerToReady(gameId, playerNumb);

        currentSuit = -1;

        Player p = players.get(playerNumb);
        cardsLeft = p.getNumberOfCards();

        // if this player won the mini round, this player will set everything up for a new mini round
        if(playerNumb == miniRoundWinner){
            boolean res = false;
            while(!res) {
                try {
                    if (Database.getReady(gameId)) {
                        Database.resetTable(gameId);
                        Database.setCurrentSuit(gameId, -1);
                        Database.setAllReadyToFalse(gameId);
                        res = true;
                    }
                }catch (DatabaseException e){
                    if(!Database.gameExists(gameId)){
                        res=true;
                    }
                }
            }
        }

        // if all cards has been played, start new round.

        if (cardsLeft == 0 && turn == -1) {
            endOfRoundHandler();
        } else { // else give the turn to the winner
            turn = miniRoundWinner;
        }
    }

    /**
     *  The method is called on when all the set rounds has been played or when a mini round has ended. Clears all the
     *  player objects from the game by deleting the party, allowing them to start/join another if all the rounds
     *  are played. Calls on prepareForBiddingRound() if there are remaining rounds to be played.
     *
     */
    private void endOfRoundHandler() {
        if(getEuropean()){
            Database.addNumberOfEuropeans(User.getUserName());
        }

        updateScore();
        if (round != totalRounds -1) {
            for (int i = 0; i < players.size(); i++) {
                players.get(i).clean();
            }

            if(playerNumb == miniRoundWinner){
                Database.setEverybodysCurrentBid(gameId);
            }
            round++;
            prepareForBiddingRound();

        }else{
            endOfGameHandler();
        }
    }

    /**
     * The method is called on when a game has ended. Gets the player number of the winner and stores the number
     * of wins in the database. Also stores number of games played to the database.
     */
    public void endOfGameHandler(){

        ArrayList<Player> winners = getWinner();
        for(Player winner : winners){
            if(playerNumb == winner.getPlayerNumb()){
                Database.addNumberOfWins(User.getUserName());
                break;
            }
        }
        Database.addNumberOfGamesPlayed(User.getUserName());

        exit = true;
    }

    /**
     * The method is called on at the start of every bidding round. The method sets everything ready for a new round
     *
     */
    public void prepareForBiddingRound(){
        cardsOnTable = new Card[players.size()];
        while(!isBiddinground && !exit) {

            // Get current bids from database
            int[] bidsFromDatabase = Database.getBids(gameId);

            if(bidsFromDatabase.length != 0 && bidsFromDatabase[0] == 0) {

                // if it is this players turn to get cards and this player has no cards
                if (playerNumb == Database.getNumberOfReadyPlayers(gameId) && players.get(playerNumb).getNumberOfCards() == 0) {

                    // if it is the first players turn to get cards, make all cards in the deck available
                    if (playerNumb == 0) {
                        Database.refreshDeckOfCards(gameId);
                    }

                    // deals cards to the player
                    try {

                        players.get(playerNumb).setHand(this.dealCard());
                        Database.setPlayerToReady(gameId, playerNumb);

                    } catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }

                // when everybody has been dealt cards, bidding round is initiated.
                if (Database.getReady(gameId)) {

                    // cleans the table that controls the highest bidder and their bid
                    highestBidderAndBid[0] = -1;
                    highestBidderAndBid[1] = -1;

                    // cleans the table that holds the player number of the teammates
                    team[0] = -1;
                    team[1] = -1;

                    // sets trump and turn
                    trump = -1;
                    turn = 0;

                    // If you are player 0, you clean the database for bidding round.
                    // and sets everyone in the database to not ready.
                    if (playerNumb == 0) {
                        Database.cleanForBiddingRound(gameId);
                        Database.setAllReadyToFalse(gameId);
                    }
                    isBiddinground = true;
                }
            }
        }
    }

    /**
     * Returns <code>true</code> if a users bid is successfully written and stored in the database.
     *
     * @param bid the number of tricks the player bids. Any bid between 0 - 13 is allowed. If the player bids 0 or
     * anything lower than the highest bidder, the bid is automatically set to -1.
     * @return <code>true</code> if the bid is successfully written to the database; <code>false</code> otherwise, or if
     * the player places a bid when it is not their turn
     */
    public boolean placebid(int bid) {

        // any bid that is not higher than the highest bid will be set as a forfeit
        if(bid <= this.highestBidderAndBid[1]){
            bid = -1;
        }

        if (turn == playerNumb) {
            return Database.setBid(bid, gameId, playerNumb);
        } else {
            return false;
        }

    }

    /**
     * Returns <code>true</code> if it is a players turn and the card chosen is allowed to be played.
     *
     * @param card The card that is played
     * @return <code>true</code>if it is this players turn and played card is allowed to play; <code>false</code>
     * otherwise.
     */
    public boolean playCard(Card card){
        if(playerNumb == miniRoundWinner){
            Database.setCurrentSuit(gameId,card.getSuit());
        }
        if(playerNumb == getTurn() && isTeamCreated()){
            if(cardAllowed(card)){
                writePlayedCard(players.get(playerNumb).playCard(card));
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the chosen card is allowed to play according to set game logic.
     *
     * @param card the played card
     * @return <code>true</code> if the played card is allowed to play; <code>false</code> otherwise.
     */
    public boolean cardAllowed(Card card){
        //Does the player have the requested card on hand?
        Card requestedCard = Database.getRequestedCard(gameId);
        if(players.get(playerNumb).haveCard(requestedCard)){
            //if so, is the card they have chosen the requested card?
            if(card.equals(requestedCard)){
                return true;
            }else{
                return false;

            }
            //if not, does the player have the current suit on hand?
        }else if(players.get(playerNumb).haveCurrentSuit(currentSuit)){
            //if so, is the card chosen the current suit?
            if(card.getSuit() == currentSuit){
                return true;
            }else {
                return false;
            }
        //if a suit is yet to be chosen or the player does not have the current suit on hand,
        //the player can play the suit of their choice.
        }else{
            return true;
        }
    }

    /**
     * Returns <code>true</code> if the card the player played is written to the database.
     *
     * @param card the played card
     * @return <code>true</code> if the card the player played is successfully written to the database;
     * <code>false</code> otherwise
    */
    public boolean writePlayedCard(Card card) {
        return Database.setPlayedCard(card, playerNumb, gameId);
    }

    /**
     * Method is called on when a game has been quit. Sets ethe exit value to true, which signalizes the thread to stop.
     * If the user initiated the game (party leader), the game then gets deleted.
     */
    public void quitGame(){
        exit = true;

        Database.setPlayerState(false, partyId, playerNumb);

        try {
            gameThread.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        if(playerNumb == 0){
            Database.deleteGame(gameId);
        }

        gameId = -1;
    }

    /**
     * Returns a selection of random cards from the database
     *
     * @return the random cards if successfully retrieved.
     * @throws NullPointerException if no card are received from the database.
     */
    public ArrayList<Card> dealCard() {

        ArrayList<Card> cards;

        if(players.size() == 3) {
            cards = Database.getRandomizedCards(gameId, 13);
        } else {
            cards = Database.getRandomizedCards(gameId, 13);
        }

        if (cards.size() == 0) {
            throw new NullPointerException("Got empty cards from database in game.dealCard() method");
        }
        return cards;
    }

    public String toString() {
        String string = "ROUND- " + +round
                + "\nTOTAL ROUNDS- " + totalRounds
                + "\nTEAM- " + "1: " + team[0] + " | 2: " + team[1]
                + "\nSTARPLAYER- " + startPlayer
                + "\nBID (currentBid)- Player: " + highestBidderAndBid[0] + " | Bid: " + highestBidderAndBid[1]
                + "\nEXIT- " + exit
                + "\nBIDDINGROUND- " + isBiddinground
                + "\nGAMEID- " + gameId
                + "\nTRUMPF- " + trump
                + "\nCURRENTSUIT- " + currentSuit
                + "\nPLAYERNUMB- " + playerNumb
                + "\nCARDSLEFT- " + cardsLeft
                + "\nSTARTER- " + playerWithRequestedCard
                + "\nTURN- " + turn
                + "\nPARTYID- " + partyId
                + "\nPLAYERS -----";

        for (Player p : players) {
            string += "\n>PlayersId: " + p.getPlayerNumb() + " | Name: " + p.getDisplayname() + "\nScore: " + p.getScore() + "\nTricks taken: " + p.getTricksTaken();
        }
        string += "\nCARDS ON TABLE ------";

        for (Card c : cardsOnTable) {
            if (c != null) {
                string += "\n>" + c.toString();
            } else {
                string += "\n>null";
            }
        }
        return string;
    }
}