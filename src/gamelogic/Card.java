/**
 * Represents a card with its associated suit and value.
 *
 * @author Team 2
 * @version 1.0
 *
 */
public class Card {
    private final int value;
    private final int suit;

    public Card (int value, int suit){
        this.value = value;
        this.suit = suit;
    }

    public int getValue(){
        return value;
    }

    public int getSuit(){
        return suit;
    }



    /**
     * Returns a comparison number of value 1 if the card that is being compared is better that the others.
     * The method is used decide which card is better based upon the current suit, the trump and the values of each card.
     * Used in {@link Game#getMiniRoundWinner}
     *
     * @param trumpSuit The suit of the chosen trump card.
     * @param currentSuit The suit of the last card played on the table
     * @param card
     * @return  -1 if this object is better than the argument, -1 if the argument is better, and 0 if both are equal, in respect to both value and relationship with current suit and trump
     */
    public int compareValue(int trumpSuit, int currentSuit, Card card){

        // if both or neither of the cards are trump
        if((this.suit == trumpSuit) == (card.getSuit() == trumpSuit)){

            //if both or neither of the cards are the current value
            if((this.suit == currentSuit) == (card.getSuit() == currentSuit)){

                //Sort after value; the cards are either both current suit, both trump or both neither
                if(value < card.getValue()){
                    return -1;
                }else if(value > card.getValue()){
                    return 1;
                }else{
                    //this should only happen if neither played trump or the current suit and the values are the same
                    return 0;
                }
            }else{

                //one card is the current suit and one card is not --> return in favor of the card that is the current suit;
                if(suit == currentSuit){
                    return 1;
                }else {
                    return -1;
                }
            }
        }else{

            //one card is the trumpf suit and one card is not --> return in favor of the card that is the trumpf suit;
            if(suit == trumpSuit){
                return 1;
            }else {
                return -1;
            }
        }
    }

    /**
     * Returns <code>true</code> if this card has the same value and suit as the argument
     *
     * <p>
     *     Method used in {@link Game#cardAllowed(Card)} and {@link Player#haveCard(Card)}.
     * </p>
     *
     * @param obj An object casted to a Card object.
     * @return <code>true</code> if the cards compared are the same; <code>false</code> otherwise
     */
    public boolean 	equals(Object obj){
        Card card = (Card) obj;
        return card.getValue() == value && card.getSuit() == suit;

    }

    /**
     * Returns <code>true</code> if a card has a valid value and suit
     *
     * @return <code>true</code> if the card is valid; <code>false</code> otherwise.
     */
    public boolean isValid(){

        if(suit >= 1 && suit <= 6) {
            if (value >= 2 && value <= 14) {
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return "Value: " + value + " Suit: " + Types.getCardSuitString(suit);
    }
}
