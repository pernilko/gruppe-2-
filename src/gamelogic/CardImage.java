import javafx.scene.image.Image;

/**
 * Distributes a matching image to each card according to its suit and value.
 *
 * @author Maria Kleppestø McCulloch
 * @see Image
 * @see Card
 *
 */

public class CardImage extends Image {
    private Card card;

    /**
     * Constructs a new card with an url containing the cards matching image.
     * The image visually shows the cards suit and value.
     *
     * @param url the resource url used to get the image by.
     * @param card the card
     */
    public CardImage(String url, Card card){
        super(url);




        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * Returns the String image url for the card it is requesting
     *
     * @param card the card object
     * @return the image url for the card
     */
    public static String getImageUrl(Card card){
        String suit = Types.getCardSuitString(card.getSuit());

        String url = "CardPack/" + suit+card.getValue() + ".png";
        return url;
    }

    /**
     * Returns an object of the current class; CardImage
     *
     * @param card the card object
     * @return the cardImage object
     */
    public static CardImage getCardImage(Card card ){
        String url = getImageUrl(card);
        return new CardImage(url,card);
    }

}
