import java.sql.*;
import java.util.ArrayList;

/**
 * Handles the communication to the database.
 *
 * @author team 2
 * @version 1.0
 */

public class Database {

    /**
     * Returns the hashed password string of a user stored in the database.
     * @param username the username of the user
     * @return hashed password
     */
    public static String getPasswordHash(String username) {
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            String sql = "SELECT password FROM user WHERE username = ?";
            pre = con.prepareStatement(sql);
            pre.setString(1, username);

            res = pre.executeQuery();
            if(res.next()) {
                return res.getString(1);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the display name of a user.
     * @param username the username
     * @return the display name
     */
    public static String getDisplayname(String username) {
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            String sql = "SELECT display_name FROM user WHERE username = ?";
            pre = con.prepareStatement(sql);
            pre.setString(1, username);
            res = pre.executeQuery();
            res.next();
            return res.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * @return a two dimensional String table with the ten users that has won the most games.
     */
    public static String[][] getTop10() {
        Connection con = null;
        Statement stmt = null;
        ResultSet res = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "SELECT display_name, number_of_wins FROM user ORDER BY number_of_wins DESC LIMIT 10";
            stmt = con.createStatement();
            res = stmt.executeQuery( sql );

            String[][] top10 = new String[10][2];
            int i = 0;
            while (res.next()) {
                top10[i][0] = res.getString( 1 );
                top10[i][1] = res.getString( 2 );
                i++;
            }
            return top10;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet( res );
            CleanUpDatabase.closeStatement( stmt );
            DatabasePool.releaseConnection( con );
        }
    }

    /**
     * Returns an ArrayList with all the players in a party.
     * @param party_id the party identification
     * @return ArrayList with all Player objects in a party.
     */
    public static ArrayList<Player> getPlayersInParty ( int party_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return null;
            }
            String sqlSentence = "SELECT p.username, p.tricks, p.player_numb, p.points, u.display_name FROM player p JOIN user u ON p.username = u.username where party_id = ? order by p.player_numb ASC;";
            ArrayList<Player> playersInParty = new ArrayList<>();

            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, party_id);
            res = pre.executeQuery();

            while (res.next()) {
                String username = res.getString("username");
                int tricks = res.getInt("tricks");
                int player_numb = res.getInt("player_numb");
                int points = res.getInt("points");
                String display_name = res.getString("display_name");
                Player player = new Player(username, display_name, player_numb);
                player.setTricksTaken(tricks);
                player.setScore(points);
                playersInParty.add(player);
            }
            return playersInParty;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns all the party objects stored in the database in an ArrayList.
     * @return all the party objects
     */
    public static ArrayList<Party> getParties() {
        ResultSet res = null;
        Statement stmt = null;
        Connection con = null;

        try {
            ArrayList<Party> allParties = new ArrayList<>();
            con = DatabasePool.getConnection();
            String sqlPartyObject = "SELECT * FROM party";

            stmt = con.createStatement();

            res = stmt.executeQuery(sqlPartyObject);
            while (res.next()) {
                Party party = new Party(res.getString("partyname"), res.getInt("party_id"));
                party.setTotalRounds(res.getInt("total_rounds"));
                allParties.add(party);
            }
            return allParties;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(stmt);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the total number of rounds of a party
     * @param party_id the party identification
     * @return the total number of rounds of a party. If SQLException returns -1
     */
    public static int getTotalRounds(int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        ResultSet res = null;
        try{
            con = DatabasePool.getConnection();
            String sql = "SELECT total_rounds FROM party WHERE party_id = ?";

            pre = con.prepareStatement(sql);
            pre.setInt(1,party_id);
            res = pre.executeQuery();
            res.next();
            return res.getInt("total_rounds");

        }catch (SQLException e){
            e.printStackTrace();
            return -1;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the game identification of a game
     * @param party_id the party identification
     * @return the game identification, if SQLException or gets empty result set returns -1
     */
    public static int getGameId(int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        ResultSet res = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "select game_id from game where party_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, party_id);
            res = pre.executeQuery();
            if(!res.isBeforeFirst()){
                return -1;
            }
            res.next();
            return res.getInt("game_id");
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns an integer table with the player numbers of the players that are on team together.
     * The first integer in the table refers to team member one.
     * The second integer in the table refers to team member two.
     * @param party_id the party identification
     * @return returns integer table with the player numbers, returns null if SQLException
     */
    public static int[] getTeamIndex ( int party_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return null;
            }
            String sql = "SELECT team_member_1, team_member_2 from game where game_id = ?";
            int[] teamIndex = {-1, -1};
            pre = con.prepareStatement(sql);
            pre.setInt(1, party_id);
            res = pre.executeQuery();

            res.next();
            int team_member_1 = res.getInt("team_member_1");
            int team_member_2 = res.getInt("team_member_2");
            teamIndex[0] = team_member_1;
            teamIndex[1] = team_member_2;
            return teamIndex;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns a deck of cards to the player
     * @param game_id the game identification the player belongs to
     * @param number_of_cards the number of cards the player is going to get
     * @return ArrayList of cards to the player that called the method. Returns null if SQLException
     */
    public static ArrayList<Card> getRandomizedCards ( int game_id, int number_of_cards){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            ArrayList<Card> cards = new ArrayList<>();
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con, false);
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return null;
            }

            String sqlsentence = "Select *from deck where free = 1 and game_id = ? order by rand() limit ?";
            pre = con.prepareStatement(sqlsentence);
            pre.setInt(1, game_id);
            pre.setInt(2, number_of_cards);

            res = pre.executeQuery();

            while (res.next()) {
                int card_suit = res.getInt("card_suit");
                int card_value = res.getInt("card_value");
                cards.add(new Card(card_value, card_suit));
            }

            String sqlSentenceUpdate = "UPDATE deck set free = 0 where card_value = ? and card_suit = ? and game_id = ?";
            pre = con.prepareStatement(sqlSentenceUpdate);
            pre.setInt(3, game_id);
            for (int i = 0; i < cards.size(); i++) {
                int card_suit = cards.get(i).getSuit();
                int card_value = cards.get(i).getValue();
                pre.setInt(1, card_value);
                pre.setInt(2, card_suit);
                pre.executeUpdate();
            }
            con.commit();
            return cards;
        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return null;
        } finally {
            CleanUpDatabase.setAutoCommit(con, true);
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns an integer table with the bids placed by the players from the database.
     * Index place represents the player number
     * The integer on the index place is the bid placed by the player.
     * @param game_id game identification
     * @return returns an integer table with the players bids. Returns null if SQLException.
     */
    public static int[] getBids ( int game_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return null;
            }
            int[] bids;
            int counter = 0;
            String sql = "SELECT current_bid FROM player JOIN game ON game.party_id = player.party_id WHERE game_id = ? ORDER BY player_numb";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            res = pre.executeQuery();

            res.last();
            int size = res.getRow();
            bids = new int[size];
            res.beforeFirst();

            res.beforeFirst();

            while (res.next()) {
                int i = res.getInt("current_bid");
                bids[counter] = i;
                counter++;
            }
            return bids;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the requested card stored in the database.
     * @param game_id the game identification
     * @return the card object stored in the database as the requested card. If SQLException returns null.
     */
    public static Card getRequestedCard ( int game_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return null;
            }
            String sqlSentence = "Select requested_card_suit, requested_card_value from game where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            res = pre.executeQuery();

            res.next();

            int requested_card_suit = res.getInt(1);
            int requested_card_value = res.getInt(2);

            return new Card(requested_card_value, requested_card_suit);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the current suit stored in the database
     * @param game_id the game identification
     * @return the current suit stored in the database, if SQLException method returns -1
     */
    public static int getCurrentSuit(int game_id){
        Connection con = null;
        ResultSet res = null;
        PreparedStatement pre = null;
        try{
            con = DatabasePool.getConnection();
            String sql = "SELECT current_suit FROM game WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1,game_id);
            res = pre.executeQuery();
            res.next();
            return res.getInt(1);
        }catch(SQLException e){
            e.printStackTrace();
            return -1;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if scores are loaded from the database and users score are updated.
     * @param username the username of the user
     * @return <code>true</code> if the users score are updated
     *         <code>false</code> if SQLException and score is not retrieved
     */
    public static boolean getScores(String username){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try{

            con = DatabasePool.getConnection();
            String sql = "select number_of_wins, games_played, number_of_europeans from user where username = ?";
            pre = con.prepareStatement(sql);
            pre.setString(1, username);
            res = pre.executeQuery();
            res.next();
            int numberOfWins = res.getInt("number_of_wins");
            User.setNumberOfWins(numberOfWins);
            int gamesPlayed = res.getInt("games_played");
            User.setNumberOfGamesPlayed(gamesPlayed);
            int numberOfEuropeans = res.getInt("number_of_europeans");
            User.setNumberOfEuropeans(numberOfEuropeans);

            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns a two dimensional String table with information about the cards the players in the game has placed.
     * For column number 0 the card value is stored
     * For column number 1 the card suit is stored
     * For column number 2 the player number is stored.
     * Information on one row represents the table state of one player.
     * @param game_id the game identification
     * @return returns a two dimensional String table with information about the cards placed on the table.
     */
    public static String[][] getGameState(int game_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return null;
            }
            CleanUpDatabase.setAutoCommit(con, false);

            String sql = "SELECT player_numb, card_suit, card_value FROM table_state JOIN player ON player.username = table_state.username WHERE game_id = ?";
            String sqlNumberOfRows = "SELECT COUNT(username) FROM table_state WHERE game_id =?";

            int row = 0;

            PreparedStatement newPre = con.prepareStatement(sqlNumberOfRows);
            newPre.setInt(1, game_id);
            ResultSet newRes = newPre.executeQuery();
            newRes.next();

            int numberOfRows = newRes.getInt(1);
            String[][] gameState = new String[numberOfRows][3];

            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            res = pre.executeQuery();

            while (res.next()) {
                String playerNumber = res.getString("player_numb");
                String card_suit = res.getString("card_suit");
                String card_value = res.getString("card_value");

                gameState[row][2] = playerNumber;
                gameState[row][1] = card_suit;
                gameState[row][0] = card_value;

                row++;
            }
            con.commit();
            return gameState;

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return null;

        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the number of ready players stored in the database
     * @param game_id the game identification
     * @return the number of ready players stored in the database, returns -1 if SQLException
     */
    public static int getNumberOfReadyPlayers(int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        ResultSet res = null;
        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return -1;
            }
            String sql = "SELECT COUNT(ready) FROM table_state WHERE game_id = ? AND ready = 1";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            res = pre.executeQuery();
            res.next();
            return res.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if all the players in a party except the party leader is ready to start a game
     * When a player in the database is set to joined, the player is ready to start the game
     * @param party_id the party identification
     * @return <code>true</code> if all the players in the party except the party leader is set to joined
     *         <code>false</code> if not all the players in the party except the party leader is set to joined or SQLException
     */
    public static boolean getReadyExceptForPartyLeader(int party_id){

        Connection con = null;
        ResultSet res = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "SELECT player_numb, joined FROM player  WHERE party_id = ?;";
            pre = con.prepareStatement(sql);
            pre.setInt(1, party_id);
            res = pre.executeQuery();

            while (res.next()) {
                int value = res.getInt("joined");
                int playerNumb = res.getInt("player_numb");
                if (value == 0 && playerNumb != 0 ) {
                    return false;
                }
            }
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if all the players belonging to the specified game in the database is set to ready.
     * @param game_id the game identification
     * @throws DatabaseException if method throws SQLException
     * @return <code>true</code> if all the players in the game is set to ready
     *         <code>false</code> if not all players in the game is set to ready
     *
     */
    public static boolean getReady(int game_id ){
        Connection con = null;
        ResultSet res = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "SELECT ready FROM table_state WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            res = pre.executeQuery();

            if (!res.isBeforeFirst()){
                throw new DatabaseException("Got Empty Result Set");
            }

            while (res.next()) {
                int value = res.getInt("ready");
                if (value == 0) {
                    return false;
                }
            }
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Got exception in getReady()");
        } finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the password of the user is changed in the database
     * @param username the username
     * @param hashValue the hash value of the new password
     * @return <code>true</code> if the password of the user is changed in the database
     *         <code>false</code> if SQLException and the database is not updated
     */
    public static boolean setNewPassword(String username, String hashValue){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "UPDATE user SET password = ? WHERE username = ?";
            boolean isolationLvl = DatabasePool.setIsolationLv(con,8);
            if(!isolationLvl){
                return false;
            }
            pre = con.prepareStatement(sql);
            pre.setString(1,hashValue);
            pre.setString(2,username);
            pre.executeUpdate();
            return true;

        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if users display name has been changed.
     * @param username the username
     * @param newDisplayname the new display name
     * @return <code>true</code> if the users display name has been changed
     *         <code>false</code> if SQLException and display name is not changed
     */
    public static boolean setDisplayname(String username, String newDisplayname){
        Connection con = null;
        PreparedStatement pre = null;
        try{
            con = DatabasePool.getConnection();
            String sql = "UPDATE user SET display_name = ? WHERE username = ?";
            boolean isolationLvl = DatabasePool.setIsolationLv(con,8);
            if(!isolationLvl){
                return false;
            }
            pre = con.prepareStatement(sql);
            pre.setString(1,newDisplayname);
            pre.setString(2,username);
            pre.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the players bid has been written in the database.
     * @param bid the bid the player placed
     * @param game_id the game identification the player belongs to
     * @param player_number the player number
     * @return <code>true</code> if the bid has been written in the database
     *         <code>false</code> if SQLException and bid has not been written in database
     */
    public static boolean setBid(int bid, int game_id, int player_number) {
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sqlSentence = "UPDATE player join game on player.party_id = game.party_id set current_bid = ? where player_numb = ? and game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, bid);
            pre.setInt(2, player_number);
            pre.setInt(3, game_id);

            pre.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the highest bid is set on the second team members
     * Uses the
     * {@link Database#setBid(int, int, int)}
     * method to set the highest bid on the second team member
     * @param player_number the player number of the second team member
     * @param game_id the game identification
     * @return <code>true</code> if the highest bid is set on the second team member
     *         <code>false</code> if SQLException and the bid is not set on the second team member
     */
    public static boolean setOtherTeamMate ( int player_number, int game_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }

            String sqlSentence = "SELECT max(current_bid) from player join game on game.party_id = player.party_id where game_id = ? group by player.party_id";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            res = pre.executeQuery();
            res.next();
            int highest_bid = res.getInt(1);

            if (!setBid(highest_bid, game_id, player_number)) {
                return false;
            }

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * <code>true</code> if the requested card is updated in the database.
     * @param card the requested card
     * @param game_id the game identification
     * @return <code>true</code> if the requested card is updated in the database.
     *         <code>false</code> if SQLException and the requested card is not updated in the database.
     */
    public static boolean setRequestedCard (Card card,int game_id){
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }
            String sql = "UPDATE game set requested_card_value = ?, requested_card_suit = ? where game_id = ?";
            int card_value = card.getValue();
            int card_suit = card.getSuit();

            pre = con.prepareStatement(sql);
            pre.setInt(1, card_value);
            pre.setInt(2, card_suit);
            pre.setInt(3, game_id);

            pre.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }

    }

    /**
     * Returns <code>true</code> if the first team member is written in the database
     * @param game_id the game identification
     * @param player_numb the players number
     * @return <code>true</code> if the first team member is written in the database
     *         <code>false</code> if SQLException and the first team member is not written in the database
     */
    public static boolean setTeamMember1 ( int game_id, int player_numb){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "UPDATE game SET team_member_1 = ? WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, player_numb);
            pre.setInt(2, game_id);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the second team member is written in the database
     * @param game_id the game identification
     * @param player_numb the players number
     * @return <code>true</code> if the second team member is written in the database
     *         <code>false</code> if SQLException and the second team member is not written in the database
     */
    public static boolean setTeamMember2 ( int game_id, int player_numb){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "UPDATE game SET team_member_2 = ? WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, player_numb);
            pre.setInt(2, game_id);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if all the players in a game is set to not ready.
     * The ready bit in the database is changed from true to false.
     * @param game_id the game identification
     * @return <code>true</code> if all the players in a game is set to false/not ready in the database
     *         <code>false</code> if SQLException and the database is not updated
     */
    public static boolean setAllReadyToFalse ( int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "UPDATE table_state SET ready = 0 WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the player is set to ready
     * @param game_id the game identification
     * @param player_numb the player number
     * @return <code>true</code> if the player is set to ready in the database
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean setPlayerToReady ( int game_id, int player_numb){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            String sql = "UPDATE table_state t JOIN player p ON t.username = p.username SET ready = 1 WHERE game_id = ? AND player_numb = ?";
            con = DatabasePool.getConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            pre.setInt(2, player_numb);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }

    }

    /**
     * Returns <code>true</code> if all the players bid is updated to 0
     * @param game_id the game identification
     * @return <code>true</code> if all the players bid in a game is updated to 0
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean setEverybodysCurrentBid ( int game_id){
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sqlSentence = "UPDATE player join game on player.party_id = game.party_id set current_bid = 0 where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the current suit is updated in the database
     * @param game_id the game identification
     * @param current_suit the current suit
     * @return <code>true</code> if the current suit in the database is updated
     *         <code>false</code> if SQLException and the database is not updated
     */
    public static boolean setCurrentSuit(int game_id,int current_suit) {
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            DatabasePool.setIsolationLv(con, 8);
            String sql = "UPDATE game SET current_suit = ? WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, current_suit);
            pre.setInt(2, game_id);
            pre.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the players new state is written in the database
     * Method updates the player table and sets the joined value to true or false, based upon the value of the first argument
     * @param playerState the players state
     * @param party_id the party identification
     * @param playerNumb the player number
     * @return <code>true</code> if the players state is updated in the database
     *         <code>false</code> if SQLException and the players state is not updated in the database
     */
    public static boolean setPlayerState(boolean playerState, int party_id, int playerNumb){
        Connection con = null;
        PreparedStatement pre = null;
        try{

            int bitPlayerState;

            if(playerState){
                bitPlayerState = 1;
            }else{
                bitPlayerState = 0;
            }
            con = DatabasePool.getConnection();

            String sqlSentence = "update player SET joined = ? WHERE party_id = ? AND player_numb = ?;";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, bitPlayerState);
            pre.setInt(2, party_id);
            pre.setInt(3, playerNumb);

            pre.executeUpdate();
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the trump value has changed in the database
     * @param trump the new trump value
     * @param game_id the game identification
     * @return <code>true</code> if trump value is updated in the database
     *         <code>false</code> if SQLException and trump is not updated
     */
    public static boolean setTrump(int trump, int game_id){
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            String sqlSentence = "UPDATE game set TRUMP = ? where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, trump);
            pre.setInt(2, game_id);
            pre.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the played card is successfully written in the database.
     * @param card the card played
     * @param playernumber the playernumber
     * @param game_id the game identification
     * @return <code>true</code> if the card object played is stored in the database
     *         <code>false</code> if SQLException and the card object is not stored in the database
     */
    public static boolean setPlayedCard(Card card, int playernumber, int game_id){
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            String sqlSentence = "update table_state t JOIN game g ON t.game_id = g.game_id JOIN player p ON g.party_id = p.party_id and t.username = p.username set card_suit = ?, card_value = ? where g.game_id = ? and player_numb = ?";
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }

            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, card.getSuit());
            pre.setInt(2, card.getValue());
            pre.setInt(3, game_id);
            pre.setInt(4, playernumber);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;

        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the score is successfully written in the database
     * @param score the players score
     * @param player_numb the players number
     * @param game_id the game identification
     * @return <code>true</code> if the players score is written in the database
     *         <code>false</code> if SQLException and players score is not written in the database
     */
    public static boolean setScore(int score, int player_numb, int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            String sql = "UPDATE player p JOIN game g ON p.party_id = g.party_id SET points = ? WHERE game_id = ? AND player_numb = ?";

            pre = con.prepareStatement(sql);
            pre.setInt(1, score);
            pre.setInt(2, game_id);
            pre.setInt(3, player_numb);
            pre.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }


    /**
     * Returns <code>true</code> if the players new trick is written in the database
     * When a player gets a trick, the players tricks in the database is incremented by one
     * @param game_id the game identification
     * @param player_numb the players number
     * @return <code>true</code> if the players trick is written in the database
     *         <code>false</code> if SQLException and the players trick is not written in the database
     */
    public static boolean addTrick ( int game_id, int player_numb){
        Connection con = null;
        PreparedStatement pre = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "UPDATE player JOIN game ON player.party_id = game.party_id SET player.tricks = player.tricks + 1 WHERE game_id = ? AND player_numb = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            pre.setInt(2, player_numb);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if total rounds is updated in the database
     * Updates the party table in the database and changes the total rounds stored there.
     * @param total_rounds the total rounds of the game
     * @param party_id the party identification
     * @return <code>true</code> if the total rounds for a party is changed
     *         <code>false</code> if SQLException and database is not update
     */
    public static boolean changeTotalRounds(int total_rounds, int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        try{
            con = DatabasePool.getConnection();
            String sql = "UPDATE party set total_rounds = ? where party_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, total_rounds);
            pre.setInt(2, party_id);
            pre.executeUpdate();
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if users number of wins is updated in the database
     * Increments number of wins by one
     * @param username the username
     * @return <code>true</code> if the users number of wins is updated in the database
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean addNumberOfWins(String username){
        ResultSet res = null;
        Connection con = null;
        PreparedStatement pre = null;
        int numberOfWins = 0;

        try {
            // Gets the number of wins for a specific player.
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con,false);

            String fetch = "SELECT number_of_wins FROM user WHERE username = ? ";
            pre = con.prepareStatement(fetch);
            pre.setString(1,username);
            res = pre.executeQuery();
            res.next();

            numberOfWins=  res.getInt(1);
            numberOfWins ++;

            //Adds the number of wins to the specific player.
            String sql = "UPDATE user SET number_of_wins = ? WHERE username = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1,numberOfWins);
            pre.setString(2,username);
            pre.executeUpdate();
            return true;

        } catch (SQLException e){
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }

    }

    /**
     * Returns <code>true</code> if users number of Europeans is updated in the database
     * Increments number of Europeans by one
     * @param username the username
     * @return <code>true</code> if the users number of Europeans is updated in the database
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean addNumberOfEuropeans(String username) {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement pre = null;
        int numbEuropeans = 0;

        try {

            // Gets the number of europeans for a specific player.
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con, false);

            String fetch = "SELECT number_of_europeans FROM user WHERE username = ? ";
            pre = con.prepareStatement(fetch);
            pre.setString(1, username);
            res = pre.executeQuery();
            res.next();

            numbEuropeans = res.getInt(1);
            numbEuropeans++;

            //Adds the number of wins to the specific player.
            String sql = "UPDATE user SET number_of_europeans = ? WHERE username = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, numbEuropeans);
            pre.setString(2, username);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if users number of games played is updated in the database
     * Increments number of games played by one
     * @param username the username
     * @return <code>true</code> if the users number of games played is updated in the database
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean addNumberOfGamesPlayed(String username) {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement pre = null;
        int gamesPlayed = 0;

        try {
            // Gets the number of games played of a specific player.
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con, false);

            String fetch = "SELECT games_played FROM user WHERE username = ? ";
            pre = con.prepareStatement(fetch);
            pre.setString(1, username);
            res = pre.executeQuery();
            res.next();

            gamesPlayed = res.getInt(1);
            gamesPlayed++;

            //Adds the number of games played to the specific player.
            String sql = "UPDATE user SET games_played = ? WHERE username = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, gamesPlayed);
            pre.setString(2, username);
            pre.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns true if a player is added to a party in the database.
     * @param username the username of the player that is added to the party
     * @param party_id the party identification
     * @return <code>true</code> if player is added to the party in the database
     *         <code>false</code> if SQLException and player is not added to the party table in the database
     */
    public static boolean addPlayerToParty (String username,int party_id){
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }
            CleanUpDatabase.setAutoCommit(con, false);
            String sqlMaxPlayerNumb = "SELECT MAX(player_numb) AS player_numb FROM player WHERE party_id =?";
            String sqlInsertPlayer = "INSERT INTO player (username, tricks, player_numb, points, party_id) VALUES(?,0,?,0,?);";

            pre = con.prepareStatement(sqlMaxPlayerNumb);
            pre.setInt(1,party_id);
            res = pre.executeQuery();
            res.next();

            String pn = res.getString("player_numb");
            if (pn != null) {
                int player_numb = Integer.parseInt(pn) + 1;
                pre = con.prepareStatement(sqlInsertPlayer);
                pre.setString(1, username);
                pre.setInt(2, player_numb);
                pre.setInt(3, party_id);
                pre.executeUpdate();
                con.commit();
                return true;

            } else {
                int player_numb = 0;
                pre = con.prepareStatement(sqlInsertPlayer);
                pre.setString(1, username);
                pre.setInt(2, player_numb);
                pre.setInt(3, party_id);
                pre.executeUpdate();
                con.commit();
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con,true);
            DatabasePool.releaseConnection(con);
        }
    }


    /**
     * Returns <code>true</code> if user is created in the database.
     * @param username users username
     * @param displayname users displayname
     * @param passwordHash users hashed password
     * @return <code>true</code> if user is created
     *         <code>false</code> if SQLException and user not created
     */
    public static boolean createNewUser(String username, String displayname, String passwordHash) {
        PreparedStatement pre = null;
        Connection con = null;

        try {

            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }

            String sql = "INSERT INTO user(username, display_name, password) VALUES(?, ?, ?);";
            pre = con.prepareStatement(sql);
            pre.setString(1, username);
            pre.setString(2, displayname);
            pre.setString(3, passwordHash);
            pre.execute();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the party identification number of the party created in the database.
     * @param partyname the name of the party
     * @param username the username that created the party
     *                 this user will be the party leader of the party created
     * @return the identification number of the party created in the database. Returns -1 if the party is not created or party with same name already exist.
     */
    public static int createParty(String partyname, String username) {
        ResultSet res = null;
        PreparedStatement pre = null;
        Connection con = null;
        Statement stmt = null;

        try {
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con, false);
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return -1;
            }

            int index = 0;
            String sqlMaxIndex = "SELECT MAX(party_id) AS party_id FROM party";
            String sqlGetPartyname = "SELECT partyname FROM party WHERE partyname ='" + partyname + "'";
            stmt = con.createStatement();
            ResultSet partynameRes = stmt.executeQuery(sqlGetPartyname);

            if (!partynameRes.next()) {
                res = stmt.executeQuery(sqlMaxIndex);
                res.next();
                index = res.getInt("party_id") + 1;

                String insertSQL = "INSERT INTO party(party_id,total_rounds,partyname)VALUES(?,10,?)";
                pre = con.prepareStatement(insertSQL);
                pre.setInt(1, index);
                pre.setString(2, partyname);
                pre.executeUpdate();

                String sql = "INSERT INTO player (username, tricks, player_numb, points, party_id) VALUES(?,0,0,0,?)";
                pre = con.prepareStatement(sql);
                pre.setString(1, username);
                pre.setInt(2, index);
                pre.executeUpdate();

                Admin.getParties().add(new Party(partyname, index));
                con.commit();
                return index;
            }
            return -1;

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return -1;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeStatement(stmt);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns the game identification of the created game.
     * Method creates all the tables in the database a game needs.
     * If there already exist a game belonging to the party, the existing tables are updated with correct information
     * Uses methods:
     * {@link Database#deleteDeckOfCards(int)}
     * {@link Database#createDeckOfCards(int, int)}
     * {@link Database#deleteGameTable(int)}
     * @param party_id the party identification
     * @return the game identification, returns -1 if something went wrong and the game is not created
     */
    public static int createGame(int party_id) {
        PreparedStatement pre = null;
        Connection con = null;
        ResultSet res = null;
        int game_id;

        try{
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if(!resIsolationLevel){
                return -1;
            }
            CleanUpDatabase.setAutoCommit(con, false);


            // checks if a game id exists on the party id given.
            String sqlSentence = "select game_id from game where party_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, party_id);
            res = pre.executeQuery();

            if (!res.isBeforeFirst() ) {

                sqlSentence = "insert into game (party_id) value (?)";
                pre = con.prepareStatement(sqlSentence);
                pre.setInt(1, party_id);
                pre.executeUpdate();

                sqlSentence = "select game_id from game where party_id = ?";
                pre = con.prepareStatement(sqlSentence);
                pre.setInt(1, party_id);
                res = pre.executeQuery();

                res.next();
                game_id = res.getInt("game_id");

            } else {
                // It already exist a game which we update with new information
                res.next();
                game_id = res.getInt("game_id");

                boolean resDeleteDeckOfCards = deleteDeckOfCards(game_id);
                boolean resDeleteTableState = deleteGameTable(game_id);

                if (!resDeleteDeckOfCards || !resDeleteTableState){
                    return -1;
                }

                sqlSentence = "update game set current_round = 0, TRUMP = -1, requested_card_value = -1, requested_card_suit = -1, team_member_1  = -1, team_member_2 = -1 where party_id = ?";
                pre = con.prepareStatement(sqlSentence);
                pre.setInt(1, party_id);
                pre.executeUpdate();

            }

            sqlSentence = "insert into table_state (game_id, username) select " + game_id + ", username from player where party_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, party_id);
            pre.executeUpdate();

            sqlSentence = "select count(party_id) from player where party_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, party_id);
            res = pre.executeQuery();

            res.next();
            int numberOfPlayers = res.getInt("count(party_id)");

            con.commit();

            boolean resCreateDeck = createDeckOfCards(game_id, numberOfPlayers);
            if(!resCreateDeck){
                return -1;
            }

            return game_id;

        } catch (SQLException e){
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return -1;
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if a deck of cards is created in the database.
     * Creates a deck of cards in the database for a game.
     * The size of the deck is dependant of how many players there are in the game.
     * @param game_id the game id the deck belongs to
     * @param numberOfPlayers number of players in the game
     * @return <code>true</code> if deck is created in the database
     *         <code>false</code> if SQLException and deck is not created
     */
    public static boolean createDeckOfCards(int game_id, int numberOfPlayers) {
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean isolationLvl = DatabasePool.setIsolationLv(con, 8);
            if (!isolationLvl) {
                return false;
            }
            String sqlSentence = "INSERT INTO deck (card_suit, card_value, game_id) VALUES (?, ?, ?)";

            CleanUpDatabase.setAutoCommit(con, false);
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(3, game_id);
            int result = 0;

            if (numberOfPlayers == 3) {
                for (int i = 1; i < numberOfPlayers + 2; i++) {
                    pre.setInt(1, i);
                    for (int j = 3; j < 15; j++) {
                        pre.setInt(2, j);
                        result += pre.executeUpdate();
                    }
                }
            } else {
                for (int i = 1; i < numberOfPlayers + 1; i++) {

                    pre.setInt(1, i);
                    for (int j = 2; j < 15; j++) {
                        pre.setInt(2, j);
                        result += pre.executeUpdate();
                    }
                }
                if (result != 13 * numberOfPlayers) {
                    return false;
                }
            }
            con.commit();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con, true);
            DatabasePool.releaseConnection(con);
        }
    }


    /**
     * Returns <code>true</code> if deleting the game and its information from the tables was successful in the database
     * Deletes the game from the tables: table_state, deck and game
     * @param game_id the game identification
     * @return <code>true</code> if deleting the game from the database was successful
     *         <code>false</code> if SQLException and database is not updated
     */
    public static boolean deleteGame(int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            String sqlDeleteTableState = "DELETE FROM table_state WHERE game_id = ?";
            String sqlDeleteDeck = "DELETE FROM deck WHERE game_id = ?";
            String sqlDeleteGame = "DELETE FROM game WHERE game_id = ?";

            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con,false);
            boolean isolationLvl = DatabasePool.setIsolationLv(con,8);
            if(!isolationLvl){
                return false;
            }

            //Deleting table_state for game_id = game_id -> deleting info about cards and ready state for players in game
            pre = con.prepareStatement(sqlDeleteTableState);
            pre.setInt(1,game_id);
            pre.executeUpdate();

            //Deleting deck for game_id = game_id
            pre = con.prepareStatement(sqlDeleteDeck);
            pre.setInt(1,game_id);
            pre.executeUpdate();

            //Deleting game for game_id = game_id
            pre = con.prepareStatement(sqlDeleteGame);
            pre.setInt(1,game_id);
            pre.executeUpdate();

            con.commit();
            return true;

        }catch (SQLException e){
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con,true);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the game table is deleted in the database
     * @param game_id the game identification
     * @return <code>true</code> if the table_state table in the database is deleted
     *         <code>false</code> if SQLException and the database is not updated
     */
    private static boolean deleteGameTable(int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        try{
            con = DatabasePool.getConnection();
            String sqlSentence = "delete from table_state where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            pre.executeUpdate();
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the deck belonging to a game is deleted in the database
     * @param game_id the game identification
     * @return <code>true</code> if the deck belonging to a game is deleted in the database
     *         <code>false</code> if SQLException and database is not updated
     */
    private static boolean deleteDeckOfCards(int game_id){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            String sqlSentence = "delete from deck where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            pre.executeUpdate();
            return true;

        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the party has been deleted from the database
     * @param party_id the party identification
     * @return <code>true</code>  if the party has been deleted from the database
     *         <code>false</code>  if SQLException and the party has not been deleted from the database
     */
    public static boolean deleteParty(int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        try{
            con = DatabasePool.getConnection();
            CleanUpDatabase.setAutoCommit(con,false);
            boolean isolationLvl = DatabasePool.setIsolationLv(con,8);
            if(!isolationLvl){
                return false;
            }

            String sqlDeletePlayers = "DELETE FROM player WHERE party_id = ?";
            String sqlDeleteParty = "DELETE FROM party WHERE party_id = ?";
            pre = con.prepareStatement(sqlDeletePlayers);
            pre.setInt(1,party_id);
            pre.executeUpdate();

            pre = con.prepareStatement(sqlDeleteParty);
            pre.setInt(1,party_id);
            pre.executeUpdate();
            con.commit();
            return true;

        }catch (SQLException e){
            e.printStackTrace();
            CleanUpDatabase.rollback(con);
            return false;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.setAutoCommit(con,true);
            DatabasePool.releaseConnection(con);
        }
    }


    /**
     * Returns <code>true</code> if the player has left the party.
     * The player leaves the party by deleting themselves from the party table in the database.
     * @param username the username
     * @return <code>true</code> If the the player successfully deletes themselves from the party in the database
     *         <code>false</code> If SQLException and the database does not get updated
     */
    public static boolean leaveParty(String username){
        Connection con = null;
        PreparedStatement pre = null;
        try {
            String sql = "DELETE FROM player WHERE username = ?";
            con = DatabasePool.getConnection();
            pre = con.prepareStatement(sql);
            pre.setString(1,username);
            pre.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the game is started
     * The game is started if the party leader of the game is set to joined in the database
     * @param party_id the party identification
     * @return <code>true</code> if the party leader in set to joined in the database
     *         <code>false</code> if party leader is not set to joined in the database
     */
    public static boolean isGameStarted(int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        ResultSet res = null;
        try{
            con = DatabasePool.getConnection();

            String sqlSentence = "SELECT joined FROM player WHERE party_id = ? && player_numb = 0";
            pre = con.prepareStatement(sqlSentence);

            pre.setInt(1, party_id);

            res = pre.executeQuery();

            res.next();

            if(res.getInt("joined") == 0){
                return false;
            }else{
                return true;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if there is still a party leader in the party stored in the database
     * @param party_id the party identification
     * @throws DatabaseException if method throws SQLException
     * @return <code>true</code> if there is still a party leader in the party
     *         <code>false</code> if there is no party leader in the party
     */
    public static boolean partyLeaderExist(int party_id){
        Connection con = null;
        PreparedStatement pre = null;
        ResultSet res = null;
        try{
            con = DatabasePool.getConnection();

            String sqlSentence = "SELECT username FROM player WHERE party_id = ? && player_numb = 0";
            pre = con.prepareStatement(sqlSentence);

            pre.setInt(1, party_id);

            res = pre.executeQuery();

            res.next();

            if(res.next()){
                return false;
            }else{
                return true;
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new DatabaseException("Something went wrong in the database");
        } finally {
            CleanUpDatabase.closeResultSet(res);
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the deck in the database is refreshed.
     * When cards in the deck is used, they are marked as 0 in the "free" column.
     * This method changes the bit value from 0 to 1 in the "free" column, marking the card as free.
     * @param game_id the game id the deck belongs to
     * @return <code>true</code> if the deck is refreshed
     *         <code>false</code> if SQLException and deck is not refreshed
     */
    public static boolean refreshDeckOfCards(int game_id) {
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean e = con.isClosed();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }

            String sqlSentence = "update deck SET free = 1 where game_id = ?";
            pre = con.prepareStatement(sqlSentence);
            pre.setInt(1, game_id);
            pre.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if the table_state in the database is reset.
     * Table_state in database is cleared by replacing all cards with the card that represents no card (suit value -1 and card value -1).
     * @param game_id the game identification
     * @return <code>true</code> if the table state in the database is reset;
     *         <code>false</code> if SQLException is thrown or a table is not reset.
     */
    public static boolean resetTable ( int game_id){
        PreparedStatement pre = null;
        Connection con = null;

        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "UPDATE table_state SET card_suit = -1, card_value = -1 WHERE game_id = ?";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id);
            pre.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            CleanUpDatabase.closeStatement(pre);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Returns <code>true</code> if cleaning the database for bidding round is successful
     * Sets the requested card suit to -1 and card value to -1 in the database.
     * Uses the
     * {@link Database#setTrump(int, int)}
     * {@link Database#resetTable(int)}
     * {@link Database#setTeamMember1(int, int)}
     * {@link Database#setTeamMember2(int, int)}
     * methods and sets the values to be -1.
     *
     * @param game_id the game identification
     * @return <code>true</code> if database has been prepared for bidding round
     *         <code>false</code> if SQLException and the database has not been prepared for bidding round
     */
    public static boolean cleanForBiddingRound ( int game_id){

        boolean resChangeTrump = setTrump(-1, game_id);

        boolean resResetTable = resetTable(game_id);

        boolean resTeammate1 = setTeamMember1(game_id, -1);
        boolean resTeammate2 = setTeamMember2(game_id, -1);

        setRequestedCard(new Card(-1, -1), game_id);
        if (!resChangeTrump || !resResetTable || !resTeammate1 || !resTeammate2) {
            return false;
        }
        return true;
    }

    /**
     * Returns <code>true</code> if the game still exists in the database
     * @param game_id the game identification
     * @throws DatabaseException if method throws SQLException
     * @return <code>true</code> if the game still exist in the database
     *         <code>false</code> if the game does not exist in the database
     */
    public static boolean gameExists(int game_id){
        Connection con = null;
        ResultSet res = null;
        PreparedStatement pre = null;
        try {
            con = DatabasePool.getConnection();
            boolean resIsolationLevel = DatabasePool.setIsolationLv(con, 8);
            if (!resIsolationLevel) {
                return false;
            }
            String sql = "SELECT game_id FROM game  WHERE game_id  = ?;";
            pre = con.prepareStatement(sql);
            pre.setInt(1, game_id );
            res = pre.executeQuery();

            if(!res.isBeforeFirst()){
                return false;
            }
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("A problem occurred in the database");
        } finally {
            CleanUpDatabase.closeStatement(pre);
            CleanUpDatabase.closeResultSet(res);
            DatabasePool.releaseConnection(con);
        }
    }

    /**
     * Disconnects the database from the connection pool
     */
    public static void disconnect(){
        DatabasePool.disconnect();
    }
}
