import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Establishes and creates connections with the mySQL database and stores them in a ArrayList.
 * Isolation levels are also set with
 *
 * @author team2
 */

public class DatabasePool{
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static String url;

    private static List<Connection> connectionPool;
    private static List<Connection> usedConnections = new ArrayList<>();

    private static final int INITIAL_POOL_SIZE = 5;

    /**
     * Creates a ArrayList with connections when the code first is initialized.
     * Reads in username and password to the database from a properties file.
     * @throws ClassNotFoundException if the driver is not found
     */
    static{
        try {
            String user = PropertiesCache.getInstance().getProperty("user");
            String password = PropertiesCache.getInstance().getProperty("password");

            url = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/"+ user +"?user="+user+"&password="+password+"&autoReconnect=true";

            Class.forName(driver);
            List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);
            for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
                pool.add(createConnection(url));
            }
            connectionPool = pool;
            
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     * Gets a connection by fetching one from the connection pool Arraylist and place it in the
     * usedConnections ArrayList
     *
     * @return the established connection
     */
    public static synchronized Connection getConnection(){
        Connection connection = connectionPool.remove(connectionPool.size()-1);
        usedConnections.add(connection);
        return connection;
    }

    /**
     * Return <code>true</code> if a released connection is successfully returned to the connection pool. Removed
     * from the usedConnections arrayList
     *
     * @param connection the connection
     * @return <code>true</code> if the release was successful; <code>false</code> otherwise.
     */
    public static synchronized boolean releaseConnection(Connection connection){
        if(connection == null){
            System.out.println("the connection was null");

        }
        boolean result = setIsolationLv(connection, 4);
        if (!result){
            System.out.println("Something went wrong with releaseConnection in the databasepool.");
        }

        connectionPool.add(connection);

        boolean result2 = usedConnections.remove(connection);

        return result2;
    }

    /**
     * Creates a connection with the database driver.
     *
     * @param url the database driver url source as a String
     * @return the connection
     */
    private static Connection createConnection(String url){
        try{
            return DriverManager.getConnection(url);
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns <code>true</code> if the connection has successfully been set a valid transaction level.
     *
     * <p>
     * Int values for transaction levels:
     * Transaction none = 0
     * Transaction read uncommitted = 1
     * Transaction read committed = 2
     * Transaction repeatable read = 4
     * Transaction serializable = 8
     * </p>
     * @param con the connection
     * @param lv the requested transaction isolation level
     * @return <code>true</code> if the isolation level requested is set.
     */

    public static boolean setIsolationLv(Connection con, int lv){
        try {
            if( con.getTransactionIsolation() != lv) {
                con.setTransactionIsolation(lv);
            }
            return true;
        }catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Disconnect a connection by removing the connection from the connection pool.
     */
    public static void disconnect(){
        int size = connectionPool.size();
        for(int i = 0; i < size; i++){

            CleanUpDatabase.closeConnection(connectionPool.remove(0));
        }
        int usedSize = usedConnections.size();
        for(int i = 0; i < usedSize; i++){
            CleanUpDatabase.closeConnection(usedConnections.remove(0));
        }
    }
}
