/**
 * This is thrown exception if there is something wrong with the database
 */

public class DatabaseException extends RuntimeException {
    public DatabaseException(){
        super();
    }

    public DatabaseException(String message){
        super(message);
    }
}
