import java.sql.*;

/**
 * Manages transactions, connections and commitment levels of the database. First and foremost used to properly close
 * database connections by handling SQL Exceptions - and to control any failed transactions by setting the
 * commitment level right.
 *
 */

public class CleanUpDatabase {

    /**
     * Returns <code>true</code> if the established connection is properly closed.
     *
     * @param connection the utilized connection pool.
     * @return <code>true</code> if the connection is properly closed; <code>false</code> otherwise.
     */
    public static boolean closeConnection(Connection connection){
        try{
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return  false;
        }
    }

    /**
     * Returns <code>true</code> if the set of results if properly closed in the database.
     *
     * @param res the set of results
     * @return <code>true</code> if the result set is closed; <code>false</code> otherwise.
     */
    public static boolean closeResultSet(ResultSet res){
        try{
            if (res != null){
                res.close();
            }
            res = null;
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns <code>true</code> if statement if properly closed in the database.
     *
     * @param stmt the SQL statement
     * @return <code>true</code> if statement if properly closed in the database; <code>false</code> otherwise.
     */
    public static boolean closeStatement(Statement stmt){
        try{
            if(stmt != null){
                stmt.close();
            }
            stmt = null;
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the connection needs to rollback to its original state
     *
     * @param connection the database connection
     * @return <code>true</code> if the connection rollbacks to its original state; <code>false</code> otherwise.
     */
    public static boolean rollback(Connection connection){
        try{
            if (connection != null && !connection.getAutoCommit()){
                connection.rollback();
            }
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the connection's commitment level is set to autocommit.
     *
     * @param connection the database connection
     * @param commitLvl the level of commitment
     * @return <code>true</code> if the connection is set to autocommit; <code>false</code> otherwise.
     */

    public static boolean setAutoCommit(Connection connection, boolean commitLvl){
        try{
            if (connection != null && !connection.getAutoCommit() == commitLvl){
                connection.setAutoCommit(commitLvl);
            }
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }
}
