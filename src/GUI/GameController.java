import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle;
import javafx.stage.Window;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class GameController implements Initializable  {

        private static ArrayList<ImageView> hand = new ArrayList<>();
        private static ArrayList<ImageView> cardsOnTable = new ArrayList<>();
        private static ArrayList<Label> userlabels = new ArrayList<>();
        private static ArrayList<Label> tricks = new ArrayList<>();
        private static ArrayList<Label> bids = new ArrayList<>();
        private static ArrayList<Label> scores = new ArrayList<>();
        private static ArrayList<Rectangle> teamBoxes = new ArrayList<>();
        private boolean stop;
        private Game game = User.getActiveParty().getGame();
        private static boolean newWindow;
        private int buttonType;
        private int BIDDING_BUTTON = 0;
        private int REQUEST_CARD_BUTTON = 1;
        private int END_GAME = 2;
        private static Button actionButton;
        private static boolean cardIsAllowed = true;
        private int errorCounter;
        private AnimationTimer timer;

        @FXML private ListView<String> suitList;
        @FXML private ListView <String> valueList;
        @FXML private Label p1, p2, p3, p4, p5; // player display names
        @FXML private Label s1, s2, s3, s4, s5; // player scores
        @FXML private Label b1, b2, b3, b4, b5; // player bids
        @FXML private Label t1, t2, t3, t4, t5; // player tricks
        @FXML private static ToggleGroup group;
        @FXML private Button butt;
        @FXML private Label info;
        @FXML private Label roundsCounter;
        @FXML private Label currentSuitTracker;
        @FXML private Label trumpTracker;
        @FXML private Label thisPlayer;
        @FXML private Label myBid;
        @FXML private Label myScore;
        @FXML private Label myTricks;

        @FXML public   ImageView i0, i1, i2 ,i3, i4, i5,i6,i7,i8,i9,i10,i11, i12, i13, i14, i15;
        @FXML public   ImageView c1, c2, c3, c4, c5;
        @FXML public   ImageView myCard;

        @FXML public Button exitEndScreen;
        @FXML private Rectangle p1box, p2box, p3box, p4box, p5box, p6box;

        @FXML private static Stage placeBidWindow;
        @FXML private static Stage reqCardWindow;
        @FXML private static Stage thisWindow;


    public void placeBid(ActionEvent event) throws IOException {

        int bid_integer;

        placeBidWindow.close();
        newWindow = false;

        String bid;

        RadioButton b = (RadioButton) group.getSelectedToggle();
        bid = b.getText();


        if(bid.equals("Pass")){
            bid_integer = -1;
        }else if(bid.equals("16")){
            if(game.getPlayers().size() == 3){
                bid_integer = 16;
            }else{
                bid_integer = 13;
            }
        }else{
            bid_integer = Integer.parseInt( bid );
        }

        game.placebid(bid_integer);

    }

    public void requestSelectedCard(ActionEvent event) throws IOException{
        reqCardWindow.close();
        int value = Integer.parseInt(valueList.getSelectionModel().getSelectedItem());
        int suit = Types.getCardValueFromString(suitList.getSelectionModel().getSelectedItem());

        Card chosenCard = new Card(value,suit);

        game.setRequestedCard(chosenCard);
    }

    public void actionButton(ActionEvent event) throws  IOException{
        if(buttonType == BIDDING_BUTTON){

                try{
                    newWindow = true;


                    placeBidWindow = loadPopUpStage("placeBid.fxml", thisWindow, false);
                    Parent root = placeBidWindow.getScene().getRoot();

                    RadioButton pass = (RadioButton) root.lookup("#pass");
                    RadioButton r1 = (RadioButton) root.lookup("#r1");
                    RadioButton r2 = (RadioButton) root.lookup("#r2");
                    RadioButton r3 = (RadioButton) root.lookup("#r3");
                    RadioButton r4 = (RadioButton) root.lookup("#r4");
                    RadioButton r5 = (RadioButton) root.lookup("#r5");
                    RadioButton r6 = (RadioButton) root.lookup("#r6");
                    RadioButton r7 = (RadioButton) root.lookup("#r7");
                    RadioButton r8 = (RadioButton) root.lookup("#r8");
                    RadioButton r9 = (RadioButton) root.lookup("#r9");
                    RadioButton r10 = (RadioButton) root.lookup("#r10");
                    RadioButton r11 = (RadioButton) root.lookup("#r11");
                    RadioButton r12 = (RadioButton) root.lookup("#r12");
                    RadioButton r13 = (RadioButton) root.lookup("#r13");
                    RadioButton r14 = (RadioButton) root.lookup("#r14");
                    RadioButton r15 = (RadioButton) root.lookup("#r15");
                    RadioButton r16 = (RadioButton) root.lookup("#r16");

                    group = new ToggleGroup();
                    pass.setToggleGroup( group );
                    r1.setToggleGroup( group );
                    r2.setToggleGroup( group );
                    r3.setToggleGroup( group );
                    r4.setToggleGroup( group );
                    r5.setToggleGroup( group );
                    r6.setToggleGroup( group );
                    r7.setToggleGroup( group );
                    r8.setToggleGroup( group );
                    r9.setToggleGroup( group );
                    r10.setToggleGroup( group );
                    r11.setToggleGroup( group );
                    r12.setToggleGroup( group );
                    r13.setToggleGroup( group );
                    r14.setToggleGroup( group );
                    r15.setToggleGroup( group );
                    r16.setToggleGroup( group );
                    pass.setSelected(true);


                    placeBidWindow.showAndWait();

                }catch (Exception h){
                    h.printStackTrace();
                }


        }else if (buttonType == REQUEST_CARD_BUTTON){
                try {
                    reqCardWindow = loadPopUpStage("requestCard.fxml", thisWindow, true);
                    reqCardWindow.showAndWait();
                }catch (Exception g){
                    g.printStackTrace();
                }
        } else if (buttonType == END_GAME ){
            endSequence(false);
        }
    }

    public void endGame(ActionEvent event) throws IOException {
        endSequence(false);
    }


    public void choseACard(MouseEvent event) {
            if (!game.isBiddingRound() && game.isTeamCreated() && game.getTurn() == game.getPlayerNumb()) {
                ImageView imageHovered = (ImageView) event.getSource();
                CardImage card = (CardImage) imageHovered.getImage();

                if (!game.playCard(card.getCard())) {
                    cardIsAllowed = false;
                }else{
                    cardIsAllowed = true;
            }
        }
    }


    public void setGame(Game gameF){
        game = gameF;
    }

    public static void setWindow(Stage window){
        thisWindow = window;
    }

    public void leaveGameScene(){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
            Scene rootScene = new Scene(root);
            rootScene.getStylesheets().add("style.css");

            thisWindow.setScene(rootScene);
            thisWindow.show();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void loadGame(){

        setUpHand();

        setActionButton();

        hideUnusedLabels();

        if(game.isBiddingRound()) cleanTable();

        Card[] currentCardsOnTable = game.getTable();
        Card displayCard;
        Player currPlayer;
        int player = game.getPlayerNumb();

        // this loop updates the gui with images for the cards on the table, players name, players bids, players tricks etc.

        for(int i = 0; i<6; i++){
            if(game.getPlayers().size() == 3){
                if(i == 1 || i == 3) {
                    i++;
                } else if(i == 5){
                    break;
                }
            }
            if(game.getPlayers().size() == 4){
                if(i == 1){
                    i++;
                } else if(i == 5){
                    break;
                }
            }
            if(game.getPlayers().size() == 5){
                if(i == 3){
                    i++;
                }
            }

            if(player == currentCardsOnTable.length){
                player = 0;
            }


            if(player == game.getTeam()[0] || player == game.getTeam()[1]){
                teamBoxes.get(i).setVisible(true);
            }

            if(player == game.getTurn()){
                userlabels.get(i).setTextFill(Color.web("#70dfe5"));
                userlabels.get(i).setStyle("-fx-font-weight: bolder ");
            } else {
                userlabels.get(i).setTextFill(Color.web("#ffffff"));
                userlabels.get(i).setStyle("-fx-font-style: Regular");
            }

            currPlayer = game.getPlayers().get(player);

            // sets display names
            userlabels.get(i).setText(currPlayer.getDisplayname());

            //sets tricks
            String trick = "Tricks: " + currPlayer.getTricksTaken();
            tricks.get(i).setText(trick);

            // set score
            String score = "Score: " + currPlayer.getScore();
            scores.get(i).setText(score);

            //set bid
            String bid;
            if(currPlayer.getBid() == -1){
                bid = "Bid: forfeited" ;
            }else{
                bid = "Bid: " + currPlayer.getBid();
            }

            bids.get(i).setText(bid);

            // set user cards
            displayCard = currentCardsOnTable[player];
            if(displayCard != null) {
                if(displayCard.getSuit() != -1) {
                    cardsOnTable.get(i).setVisible(true);
                    cardsOnTable.get(i).setImage(CardImage.getCardImage(displayCard));
                }else{
                    cardsOnTable.get(i).setVisible(false);
                }
            }
            player++;

        }
    }


    private  void hideUnusedLabels(){
        for(int i = 0; i < 6; i++){
            if(game.getPlayers().size() == 3){
                if(i == 0 || i == 2 || i == 4) {
                    i++;
                }
            }
            if(game.getPlayers().size() == 4){
                if(i == 0){
                    i++;
                } else if(i == 2){
                    i = 5;
                }
            }
            if(game.getPlayers().size() == 5){
                if(i == 0){
                    i = 3;
                } else if(i == 3){
                    break;
                }
            }
            userlabels.get(i).setVisible(false);
            scores.get(i).setVisible(false);
            bids.get(i).setVisible(false);
            tricks.get(i).setVisible(false);
        }
    }

    public void setActionButton(){
        actionButton.setVisible(false);

        if(game.isBiddingRound() && (game.getTurn() == game.getPlayerNumb())){
            buttonType = BIDDING_BUTTON;
            actionButton.setText("Place Bid");
            actionButton.setVisible(true);
        }else if(!game.isTeamCreated() && (game.getTurn() == game.getPlayerNumb())){
            buttonType = REQUEST_CARD_BUTTON;
            actionButton.setText("Request Card");
            actionButton.setVisible(true);
        }
    }

    public void getUserArrayLists(){
        // add the ImageViewer.object that displays a players hand to a static ArrayList
        hand.add(i0);
        hand.add(i1);
        hand.add(i2);
        hand.add(i3);
        hand.add(i4);
        hand.add(i5);
        hand.add(i6);
        hand.add(i7);
        hand.add(i8);
        hand.add(i9);
        hand.add(i10);
        hand.add(i11);
        hand.add(i12);
        hand.add(i13);
        hand.add(i14);
        hand.add(i15);

        // add all Labels that displays a players name  to an array list
        userlabels.add(thisPlayer);
        userlabels.add(p1);
        userlabels.add(p2);
        userlabels.add(p3);
        userlabels.add(p4);
        userlabels.add(p5);

        // add all ImageViews Objects that displays the cards played to an array list
        cardsOnTable.add(myCard);
        cardsOnTable.add(c1);
        cardsOnTable.add(c2);
        cardsOnTable.add(c3);
        cardsOnTable.add(c4);
        cardsOnTable.add(c5);

        // add all Labels that displays tricks
        tricks.add(myTricks);
        tricks.add(t1);
        tricks.add(t2);
        tricks.add(t3);
        tricks.add(t4);
        tricks.add(t5);

        // add all Labels that displays bids
        bids.add(myBid);
        bids.add(b1);
        bids.add(b2);
        bids.add(b3);
        bids.add(b4);
        bids.add(b5);


        // add all Labels that displays scores
        scores.add(myScore);
        scores.add(s1);
        scores.add(s2);
        scores.add(s3);
        scores.add(s4);
        scores.add(s5);

        teamBoxes.add(p1box);
        teamBoxes.add( p2box );
        teamBoxes.add( p3box);
        teamBoxes.add( p4box );
        teamBoxes.add( p5box );
        teamBoxes.add( p6box );
    }



    private void changeInfo(){

        if(info != null){
            if(game.isBiddingRound()){
                if(game.myTurn()){
                    info.setText("Its your turn - press the button to place your bid");
                }else {
                    info.setText("Bidding Round");
                }
                if(!(info.isVisible())){
                    info.setVisible(true);
                }
            }else if(!(game.isTeamCreated())) {
                if (game.getPlayerNumb() == game.getTeam()[0]) {
                    info.setText("You won bidding round. Please request card");
                } else {
                    info.setText("Bidding round over. Waiting for player to request card from another player");
                }
            }else if (!cardIsAllowed) {
                if(game.getPlayerNumb() == game.getTurn()) {
                    info.setText("You can't play that card. Choose a card with the same suit as current suit");
                }
                if(!(info.isVisible())) {
                    info.setVisible(true);
                }
            }else{
                if(info.isVisible()){
                    info.setVisible(false);
                }
            }

        }

        if(roundsCounter != null) {
            for (int i = 1; i < game.getTotalRounds() + 1; i++) {
                roundsCounter.setText("Round: " + game.getRound() + "/" + game.getTotalRounds());
            }
        }

        if (currentSuitTracker != null){
           int i = game.getCurrentSuit();
           if(i != -1 && i != 0) {
               currentSuitTracker.setText("Current suit: "+Types.getCardSuitString(i));
           }else{
               currentSuitTracker.setText("Current suit: ");
           }
        }


        if(game.getTrump() != 0 && game.getTrump() != -1){
            if(trumpTracker != null){
                trumpTracker.setText("Trump: "+Types.getCardSuitString(game.getTrump()));
            }
        }else {
            if(trumpTracker != null) {
                trumpTracker.setText("Trump: ");
            }
        }
    }

    public Stage loadPopUpStage(String file, Window owner, boolean addStyle)throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(file));
        Stage stg = new Stage();
        Scene sceneRoot = new Scene(root);
        if(addStyle){
            sceneRoot.getStylesheets().add("style.css");
        }

        stg.setScene(sceneRoot);
        stg.initModality( Modality.APPLICATION_MODAL );
        stg.initOwner(owner);
        return stg;
    }

    public void setUpHand(){
        ArrayList<Card> cardsHand = game.getPlayerHand();

        CardImage img;
        Card card;

        for(int i = 0; i<hand.size(); i++){
            if(hand.get(i) != null) {
                hand.get(i).setCursor(Cursor.CLOSED_HAND);
                hand.get(i).setVisible(false);
                int f = i;
                hand.get(i).setOnMouseEntered(event -> {
                    if(!game.isBiddingRound()) {
                        hand.get(f).setY(hand.get(f).getY() - 40);
                    }
                });

                    hand.get(i).setOnMouseExited(event -> {
                        if(!game.isBiddingRound()) {
                            hand.get(f).setY(hand.get(f).getY() + 40);
                        }
                    });

            }
        }

        for(int i = 0; i< cardsHand.size(); i++){
            card = cardsHand.get(i);
            img = CardImage.getCardImage(card);

            hand.get(i).setImage(img);
            hand.get(i).setVisible(true);
        }
    }

    public synchronized  void endSequence(boolean showEndCard) {

        stop = true;
        timer.stop();

        try {
            User.leaveParty();
        }catch (Exception e){
            e.printStackTrace();
        }
        leaveGameScene();
    }


    public void cleanTable(){

            for(ImageView card : cardsOnTable){
                if(card != null) {
                    card.setVisible(false);
                }
            }

            for(Rectangle box : teamBoxes){
                if(box != null) {
                    box.setVisible(false);
                }
            }
    }

    private String getWinnersAsString(ArrayList<Player> winners){
        Player winner = winners.get(0);
        String winString;

        if(winner.getPlayerNumb() == game.getPlayerNumb()){
            winString = "Congratulation! You";
        }else{
            winString = winner.getDisplayname();
        }


        String differentiators;

        for(int i = 1; i<winners.size(); i++){
             winner = winners.get(i);
            if(i == winners.size()-1){
                differentiators = " and ";
            }else{
                differentiators = ", ";
            }

            if(winner.getPlayerNumb() ==  game.getPlayerNumb()){
                winString = "Congratulation! " + winString + differentiators + "You";
            }else{
                winString += differentiators + winner.getDisplayname();
            }
        }

        return winString + " won!";
    }


    @Override
    public void initialize(URL location, ResourceBundle resources)  {
        if(butt != null){
            actionButton = butt;
        }
        getUserArrayLists();

        if (!stop) {

            timer = new AnimationTimer() {
                @Override
                public void handle(long now) {
                    if (!stop) {

                        if (game.isEnded()) {
                            if (!stop) {
                                stop = true;
                                timer.stop();

                                try {
                                    cleanTable();
                                    if(info != null) {
                                        info.setVisible(true);
                                        info.setText(getWinnersAsString(game.getWinner()));
                                    }
                                   buttonType = END_GAME;
                                   actionButton.setText("End Game");
                                   actionButton.setVisible(true);
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                errorCounter++;
                            }
                            if (errorCounter > 30) {
                                timer.stop();
                            }
                        } else {
                            if (!newWindow) {
                                try {
                                    loadGame();
                                    changeInfo();
                                    errorCounter = 0;

                                } catch (NullPointerException e) {
                                    errorCounter++;
                                    if (errorCounter < 20) {
                                        timer.stop();
                                        stop = true;
                                        endSequence(false);
                                    }
                                }
                            }
                        }
                    } else {
                        timer.stop();
                    }
                }
            };
        }else{
            endSequence(true);
        }
        timer.start();
    }
}






