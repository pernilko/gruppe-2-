import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class gameRegularPlayerController implements Initializable {

    @FXML private TableView<Player> playerTableView = new TableView<Player>();
    @FXML private TableColumn<Player, String> playerViewColumn = new TableColumn<Player, String>("Party members");
    @FXML private Label readyLabel;
    @FXML private Button readyButton;
    private static Stage win;
    @FXML private Label rounds;
    private ObservableList<Player> obsListe;
    private AnimationTimer timer;
    private Party selectedParty = User.getActiveParty();


    public void setWindow(Stage window){
        win = window;
    }

    public void selectedParty(){
        selectedParty.loadPlayers();
        selectedParty.startInfoPuller();
    }

    private void loadRounds(int totalRounds) {
        rounds.setText( "Number of Rounds: "+ totalRounds);
    }


    public void startGame()throws IOException {
        selectedParty.createGame(selectedParty.getPlayerNumberOfUser());
        timer.stop();
        Parent root = FXMLLoader.load(getClass().getResource("game.fxml"));
        Scene rootScene = new Scene(root);
        rootScene.getStylesheets().add("style.css");



        GameController.setWindow(win);
        win.setScene(rootScene);

        win.setFullScreenExitKeyCombination( KeyCombination.NO_MATCH );


        win.show();

    }

    public void loadList(){
        playerViewColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("displayname"));
        playerTableView.setItems(fetchPlayers());
        playerTableView.getColumns().setAll(playerViewColumn);

    }

    public void backToMenu()throws IOException{
        Admin.getLoginUser().leaveParty();
        Parent root = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
        Scene rootScene = new Scene(root);

        Label username = (Label) root.lookup( "#username" );
        username.setText(User.getDisplayname());

        rootScene.getStylesheets().add("style.css");
        win.setScene(rootScene);
        win.show();
        timer.stop();
    }


    public ObservableList<Player> fetchPlayers(){
        ArrayList<Player> players;
        players = selectedParty.getPlayers();
        ObservableList<Player> playerObservableList = FXCollections.observableArrayList();
        for (int i = 0; i < players.size(); i++) {
            playerObservableList.add(players.get(i));
        }
        obsListe = playerObservableList;
        return playerObservableList;
    }

    public void setReady(){
        Database.setPlayerState(true, selectedParty.getParty_id(), selectedParty.getPlayerNumberOfUser());
        readyButton.setVisible(false);
        readyLabel.setVisible(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if(!selectedParty.isInfoPullerAlive()){
                    try{
                        backToMenu();

                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                loadList();
                loadRounds(selectedParty.updateTotalRounds());;
                if(selectedParty.isGameCreated()){
                    selectedParty.stopInfoPuller();
                    try{
                        startGame();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
        };
        timer.start();
    }
}
