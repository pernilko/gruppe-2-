import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class registerUserController implements Initializable {

    private static boolean regOK;
    @FXML private TextField userField;
    @FXML private TextField displayNameField;
    @FXML private PasswordField passwordField;
    @FXML private PasswordField passwordRepeatedField;
    @FXML private Label infoMessage;
    @FXML private Label registrationFailed;

    public void registerUser(ActionEvent event)throws IOException {

        if(Admin.samePassword(passwordField.getText(),passwordRepeatedField.getText())) {
            regOK = Admin.createUser(userField.getText(), displayNameField.getText(), passwordField.getText());
            if (regOK) {
                Parent registerParent = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
                Scene registerScene = new Scene(registerParent);
                registerScene.getStylesheets().add("style.css");

                Label username = (Label) registerParent.lookup("#username");
                username.setText(User.getDisplayname());
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(registerScene);
                window.show();

            } else {
                infoMessage.setVisible(true);
                registrationFailed.setText("Registration failed!");
                registrationFailed.setVisible(true);
            }
        }else{
            registrationFailed.setText("Passwords do not match!");
            registrationFailed.setVisible(true);
        }
    }
    public void logOut(ActionEvent event) throws IOException{
        Parent cancelParent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene cancelScene = new Scene(cancelParent);
        cancelScene.getStylesheets().add("style.css");
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(cancelScene);
        window.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
