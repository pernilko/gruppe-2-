import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private static boolean loginOK;
    @FXML private TextField userField;
    @FXML private PasswordField passwordField;
    @FXML public Label ErrorMessage;

    @FXML
    public void onEnter(ActionEvent event) throws IOException {
        loginToGame(event);
    }

    public void loginToGame(ActionEvent event) throws IOException {
        Parent gameViewParent = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
        String usernameRead = userField.getText();
        String passwordRead = passwordField.getText();
        if(usernameRead != null && passwordRead != null){
            loginOK = Admin.login(usernameRead, passwordRead);
            if(loginOK){
                Label username = (Label) gameViewParent.lookup( "#username" );
                username.setText(User.getDisplayname());
                Scene gameViewScene = new Scene(gameViewParent);
                gameViewScene.getStylesheets().add("style.css");
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(gameViewScene);
                window.show();
            }else {
                Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
                String errorLogin ="Wrong username or password. Try Again";
                Label lblData = (Label) root.lookup( "#ErrorMessage" );
                lblData.setText(errorLogin);
                loginOK = false;

                Scene rootScene = new Scene(root);
                rootScene.getStylesheets().add("style.css");
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(rootScene);
                window.show();
            }
        }
    }

    public void registerNewUserPage(ActionEvent event)throws IOException {
        Parent registerViewParent = FXMLLoader.load(getClass().getResource("registerUser.fxml"));
        Scene registerViewScene = new Scene(registerViewParent);
        registerViewScene.getStylesheets().add("style.css");
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(registerViewScene);
        window.show();

    }
    public void initialize(URL location, ResourceBundle resources) {
    }
}

