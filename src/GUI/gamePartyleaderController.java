import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCombination;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class gamePartyleaderController implements Initializable {

    private AnimationTimer timer;
    private Party selectedParty = User.getActiveParty();
    private ObservableList<Player> obsListe;
    public static Party party = User.getActiveParty();
    Stage stage;
    Parent root;
    @FXML private TableView<Player> tableViewGPL = new TableView<Player>();
    @FXML private Button ok;
    @FXML private Label getName;
    @FXML private ChoiceBox<Integer> gameSettingsChoiceBox;
    @FXML private TableColumn<Player, String> partyLeaderPlayerViewColumn = new TableColumn<>("Party members");
    @FXML private Button startGame;
    @FXML private Label rounds;


    public void startGameButton(ActionEvent event)throws IOException {

        party.createGame(0);
        System.out.println("Kom ned her");

        Parent root = FXMLLoader.load(getClass().getResource("game.fxml"));
        Scene rootScene = new Scene(root);

        rootScene.getStylesheets().add("style.css");

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(rootScene);

        GameController.setWindow(window);

        window.setFullScreenExitKeyCombination( KeyCombination.NO_MATCH );
        window.setFullScreen( false );

        window.show();

    }

    public void gameSettingsOk(ActionEvent event){
        int roundsChosen = gameSettingsChoiceBox.getValue();

        party.setTotalRounds(roundsChosen);
    }

    public void backToMenu(ActionEvent event) throws IOException{
        Admin.getLoginUser().deleteParty();
        Parent root = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
        Scene rootScene = new Scene(root);

        Label username = (Label) root.lookup( "#username" );
        username.setText(User.getDisplayname());

        rootScene.getStylesheets().add("style.css");

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(rootScene);

        window.show();
    }

    public void loadList(){
        partyLeaderPlayerViewColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("displayname"));
        tableViewGPL.setItems(fetchPlayers());
        tableViewGPL.getColumns().setAll(partyLeaderPlayerViewColumn);

    }

    public ObservableList<Player> fetchPlayers(){
        ArrayList<Player> players;
        players = selectedParty.getPlayers();
        ObservableList<Player> playerObservableList = FXCollections.observableArrayList();
        for (int i = 0; i < players.size(); i++) {
            playerObservableList.add(players.get(i));
        }

        obsListe = playerObservableList;
        return playerObservableList;
    }

    private void loadStartGameButton() {
        startGame.setVisible(true);

    }

    private void loadRounds(int roundsChosen){
        rounds.setText("Number of Rounds: "+ roundsChosen);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                loadList();
                loadRounds(party.getTotalRounds());
                if(PartyThread.getReadyButton()) {
                    loadStartGameButton();
                }
            }
        };
        timer.start();

    }




}

