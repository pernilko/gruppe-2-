import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;

public class profileSiteController implements Initializable {

    @FXML private Label displayName;
    @FXML private Label numberOfWins;
    @FXML private Label gamesPlayed;
    @FXML private Label numberOfEuropeans;
    @FXML private Label confirmationMessageDisplayname;
    @FXML private Label confirmationMessagePassword;
    @FXML private TextField displayname;
    @FXML private PasswordField password;
    @FXML private PasswordField oldPassword;


    public void returnToMainMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
        Scene rootScene = new Scene(root);

        Label username = (Label) root.lookup( "#username" );
        username.setText(User.getDisplayname());

        rootScene.getStylesheets().add("style.css");
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(rootScene);
        window.show();
    }

    public void logOut(ActionEvent event) throws IOException{
        Admin.getLoginUser().logout();
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene rootScene = new Scene(root);
        rootScene.getStylesheets().add("style.css");
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(rootScene);
        window.show();
    }

    public void changeDisplayname(){
        confirmationMessageDisplayname.setVisible(false);
        String newDisplayName = displayname.getText();
        if(User.changeDisplayName(newDisplayName)){
            displayname.setText("");
            confirmationMessageDisplayname.setVisible(true);
            displayName.setText(User.getDisplayname());
        }else{
            confirmationMessageDisplayname.setText("Error has occurred. Display name did not change.");
        }
    }

    public void changePassword()throws InvalidKeySpecException, NoSuchAlgorithmException {
        if(User.changePassword(oldPassword.getText(),password.getText())){
            confirmationMessagePassword.setVisible(true);
            oldPassword.setText("");
            password.setText("");
        }else{
            confirmationMessagePassword.setText("Error has occurred. Password did not change.");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        User user = Admin.getLoginUser();
        displayName.setText(User.getDisplayname());
        numberOfWins.setText("Number of wins: " + user.getNumberOfWins());
        numberOfEuropeans.setText("Number of Europeans: " + user.getNumberOfEuropeans());
        gamesPlayed.setText("Games played: " + user.getNumberOfGamesPlayed());
    }
}
