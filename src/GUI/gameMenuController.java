import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controls the graphic user interface of the game. Each button is managed here.
 * Uses FXML files to set up the interfaces
 */

public class gameMenuController implements Initializable {

    @FXML private TableView<Party> tableView = new TableView<Party>();
    @FXML private TableColumn<Party, String> partyNameColumn =
            new TableColumn<Party, String>("Parties you can join");
    @FXML private static Stage popup;
    @FXML private Label Player0, Player1, Player2, Player3, Player4, Player5, Player6, Player7, Player8, Player9;
    @FXML private Label Score0, Score1, Score2, Score3, Score4, Score5, Score6, Score7, Score8, Score9;
    @FXML private Label warningMessage;
    @FXML private Label joinPartyError;
    @FXML private Label username;
    @FXML private Stage stage;
    @FXML private TextField partyName;
    @FXML private Button ok;
    @FXML private static Stage main;
    private ObservableList<Party> obsListe;



    public void createNewParty(ActionEvent event) throws IOException {
        Parent setNameParent = FXMLLoader.load(getClass().getResource("setPartyName.fxml"));

        main = (Stage)((Node)event.getSource()).getScene().getWindow();

        popup = new Stage();
        Scene setPartyName = new Scene(setNameParent);
        setPartyName.getStylesheets().add("style.css");
        popup.setScene(setPartyName);
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.initOwner( ((Node)event.getSource()).getScene().getWindow() );
        popup.showAndWait();
    }

    public void setPartyName(ActionEvent event)throws IOException {
        String output = partyName.getText();
        popup.close();
        if(Admin.getLoginUser().createParty(output) == null){
            warningMessage.setVisible(true);
        }else{
            popup.close();

            Parent root = FXMLLoader.load(getClass().getResource("gamePartyLeader.fxml"));
            if(event.getSource() == ok) {
                stage = (Stage) ok.getScene().getWindow();

                Label lblData = (Label) root.lookup("#getName");
                lblData.setText(output);
            }

            User.getActiveParty().startInfoPuller();

            Scene rootScene = new Scene(root);
            rootScene.getStylesheets().add("style.css");
            stage = main;
            stage.setScene(rootScene);
            stage.show();
        }
    }

    public void closeSetPartyNamePopup(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("setPartyName.fxml"));

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }

    public void setTutorialButton(ActionEvent event) throws IOException {
        Parent tutorialParent = FXMLLoader.load(getClass().getResource("tutorialSite.fxml"));
        Scene tutorialScene = new Scene(tutorialParent);


        tutorialScene.getStylesheets().add("style.css");
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene( tutorialScene );
        window.show();
    }

    public void setSettingsButton(ActionEvent event) throws IOException {
        Parent setSettingsParent = FXMLLoader.load(getClass().getResource("settingSite.fxml"));

        popup = new Stage();
        Scene settingsSite = new Scene (setSettingsParent);
        popup.setScene(settingsSite);
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.initOwner( ((Node)event.getSource()).getScene().getWindow() );
        popup.showAndWait();
    }

    public void setProfileButton(ActionEvent event) throws IOException {
        Parent profileParent = FXMLLoader.load(getClass().getResource("profileSite.fxml"));
        Scene profileScene = new Scene(profileParent);
        profileScene.getStylesheets().add("style.css");
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(profileScene);
        window.show();
    }

    public void setTop10Button(ActionEvent event) throws IOException {
        Parent to10Parent = FXMLLoader.load(getClass().getResource("top10Site.fxml"));
        String[][] top10Table = Database.getTop10();

        Player0 = (Label) to10Parent.lookup( "#Player0" );
        Player1 = (Label) to10Parent.lookup( "#Player1" );
        Player2 = (Label) to10Parent.lookup( "#Player2" );
        Player3 = (Label) to10Parent.lookup( "#Player3" );
        Player4 = (Label) to10Parent.lookup( "#Player4" );
        Player5 = (Label) to10Parent.lookup( "#Player5" );
        Player6 = (Label) to10Parent.lookup( "#Player6" );
        Player7 = (Label) to10Parent.lookup( "#Player7" );
        Player8 = (Label) to10Parent.lookup( "#Player8" );
        Player9 = (Label) to10Parent.lookup( "#Player9" );

        Score0 = (Label) to10Parent.lookup( "#Score0" );
        Score1 = (Label) to10Parent.lookup( "#Score1" );
        Score2 = (Label) to10Parent.lookup( "#Score2" );
        Score3 = (Label) to10Parent.lookup( "#Score3" );
        Score4 = (Label) to10Parent.lookup( "#Score4" );
        Score5 = (Label) to10Parent.lookup( "#Score5" );
        Score6 = (Label) to10Parent.lookup( "#Score6" );
        Score7 = (Label) to10Parent.lookup( "#Score7" );
        Score8 = (Label) to10Parent.lookup( "#Score8" );
        Score9 = (Label) to10Parent.lookup( "#Score9" );

        Player0.setText( top10Table[0][0]);
        Player1.setText( top10Table[1][0]);
        Player2.setText( top10Table[2][0]);
        Player3.setText( top10Table[3][0]);
        Player4.setText( top10Table[4][0]);
        Player5.setText( top10Table[5][0]);
        Player6.setText( top10Table[6][0]);
        Player7.setText( top10Table[7][0]);
        Player8.setText( top10Table[8][0]);
        Player9.setText( top10Table[9][0]);

        Score0.setText( top10Table[0][1]);
        Score1.setText( top10Table[1][1]);
        Score2.setText( top10Table[2][1]);
        Score3.setText( top10Table[3][1]);
        Score4.setText( top10Table[4][1]);
        Score5.setText( top10Table[5][1]);
        Score6.setText( top10Table[6][1]);
        Score7.setText( top10Table[7][1]);
        Score8.setText( top10Table[8][1]);
        Score9.setText( top10Table[9][1]);

        Scene top10Scene = new Scene(to10Parent);
        to10Parent.getStylesheets().add("style.css");

        username.setText(User.getDisplayname());

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(top10Scene);
        window.show();
    }

    public void backToMenu(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("gameMenu.fxml"));
        Scene rootScene = new Scene(root);
        rootScene.getStylesheets().add("style.css");

        Label username = (Label) root.lookup( "#username" );
        username.setText(User.getDisplayname());

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(rootScene);

        window.show();
    }

    public void logOut(ActionEvent event) throws IOException{
        if(User.getActiveParty() != null){
            User.leaveParty();
        }
        User.logout();

        Parent cancelParent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene cancelScene = new Scene(cancelParent);
        cancelScene.getStylesheets().add("style.css");

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(cancelScene);
        window.show();
    }

    public void viewRegularPlayerParty(ActionEvent event) throws IOException {
        joinPartyError.setText( "Failed to join party!" );
        joinPartyError.setVisible(false);
        Party party = tableView.getSelectionModel().getSelectedItem();
        if(Admin.getLoginUser().joinParty(party.getParty_id())){
            party.loadPlayers();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("regularPlayer.fxml"));
            Parent vr = loader.load();

            Scene vrS = new Scene(vr);
            vrS.getStylesheets().add("style.css");
            gameRegularPlayerController controller = loader.getController();
            controller.selectedParty();
            controller.loadList();
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            controller.setWindow(window);

            Label lbl = (Label) vr.lookup("#getName");
            lbl.setText(tableView.getSelectionModel().getSelectedItem().getPartyName());

            window.setScene(vrS);
            window.show();
        }else{
            //display a message, failed to join party
            joinPartyError.setVisible(true);
        }
    }

    public void loadParties(){
        partyNameColumn.setCellValueFactory(new PropertyValueFactory<Party, String>("partyName"));
        partyNameColumn.getStyleClass().add("ColumnHeader");
        tableView.setItems(getParties());
        tableView.getColumns().setAll(partyNameColumn);
    }

    public ObservableList<Party> getParties(){
        ArrayList<Party> parties = Database.getParties();
        ObservableList<Party> partyName = FXCollections.observableArrayList();
        for (int i = 0; i < parties.size(); i++) {
            partyName.add(parties.get(i));
        }
        obsListe = partyName;
        return obsListe;
    }

    public void OKbutton(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("settingSite.fxml"));
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }

    public void cancelButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("settingSite.fxml"));
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(username != null){
            username.setText(User.getUserName());
        }
        loadParties();
    }
}
