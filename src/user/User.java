import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Handles information and statistics surrounding a user. Keeps track of and sorts out the users interaction
 * aspects in the game
 *
 * @author team 2
 * @version 1.0
 */

public class User  {
    private static  String username;
    private static String displayname;
    private static int numberOfEuropeans;
    private static int numberOfGamesPlayed;
    private static int numberOfWins;
    private static Party activeParty = null;

    /**
     * Constructs a new user with user- and display name. Utilises the username that has already been created
     * in the Admin class to fetch statistics stored on that user.
     *
     * @param username the users chosen username.
     * @param displayname the users chosen display name
     */
    public User(String username, String displayname){
        User.username = username;
        User.displayname = displayname;
        if(!Database.getScores(username)){
            System.out.println("Failed to read/write scores to user");
        }
    }

    /**
     * Returns the users usename.
     *
     * @return the username
     */
    public static String getUserName() {return username;}

    /**
     * Returns the users display name.
     *
     * @return the display name.
     */
    public static String getDisplayname() {return displayname;}

    /**
     * Returns statistics of the users number of Europeans collected in the game.
     *
     * @return the number of Europeans.
     */
    public static int getNumberOfEuropeans(){return numberOfEuropeans;}

    /**
     * Returns statistics of the users total number of games played.
     *
     * @return the total number of games played.
     */
    public static int getNumberOfGamesPlayed(){return numberOfGamesPlayed;}

    /**
     *  Returns statistics of the users total number of wins in the game.
     *
     * @return the total number of wins
     */
    public static int getNumberOfWins() {return numberOfWins;}

    /**
     * Returns parties that are currently active in the game
     *
     * @return currently active parties
     */
    public static Party getActiveParty() {return activeParty;}

    /**
     * Sets the users number of wins
     *
     * @param numberOfWinsRead the new number of wins the previous one is set to
     */
    public static void setNumberOfWins(int numberOfWinsRead){numberOfWins = numberOfWinsRead;}

    /**
     * Sets the users number of europeans
     *
     * @param numberOfEuropeansRead the new number of europeans the previous one is set to
     */
    public static void setNumberOfEuropeans(int numberOfEuropeansRead){numberOfEuropeans = numberOfEuropeansRead;}

    /**
     * Sets the users number of played games
     *
     * @param numberOfGamesPlayedRead the new number of games played the previous one is set to
     */
    public static void setNumberOfGamesPlayed(int numberOfGamesPlayedRead){numberOfGamesPlayed = numberOfGamesPlayedRead;}

    public static void logout(){
        username = null;
        displayname = null;
        numberOfEuropeans = 0;
        numberOfGamesPlayed = 0;
        numberOfWins = 0;
        activeParty = null;
    }

    /**
     * Returns <code>true</code> if the display name is successfully changed.
     *
     * @param newDisName the new display name
     * @return <code>true</code> if display name is successfully changed; <code>false</code> otherwise.
     */
    public static boolean changeDisplayName(String newDisName){
        if(newDisName.length() > 0 && newDisName.length() <= 20){
            if(Database.setDisplayname(getUserName(),newDisName)){
                displayname = newDisName;
                return true;
            }
        }
        return false;
    }

    /**
     * Returns <code>true</code> if the password is successfully changed.
     *
     * @param oldPassword the users old password
     * @param newPassword the user new password
     * @return <code>true</code> if the password is successfully changed; <code>false</code> otherwise.
     * @throws InvalidKeySpecException if the received KeySpecs type is not recognized
     * @throws NoSuchAlgorithmException if the particular requested cryptic algorithm is not available
     */
    public static boolean changePassword(String oldPassword, String newPassword) throws InvalidKeySpecException, NoSuchAlgorithmException {
        boolean passwordOK = HashWizard.validatePassword(oldPassword, Database.getPasswordHash(username));
        if(passwordOK) {
           String passwordHash = HashWizard.generateHash(newPassword);
           if (Database.setNewPassword(getUserName(), passwordHash)) {
               return true;
           }
        }
        return false;
    }

    /**
     * Creates a new party in the database. The object is also added as an object to the array-list of
     * parties from Admin.
     *
     * @param partyName the name of the party created
     * @return the created party as an object from the arraylist of Parties in Admin. Returns null if it fails to create
     * a party
     */
    public Party createParty(String partyName) {
        boolean refreshOK = Admin.refreshParties();
        if (partyName.length() > 0 && partyName.length() <= 20 && refreshOK) {
            int partyId = Database.createParty(partyName, getUserName()); //returns the party index
            Admin.refreshParties();
            int index = Admin.getIndexWithPartyId(partyId); //returns index in parties arraylist in admin

            activeParty = Admin.getParties().get(index);//gets the party the player belongs to
            activeParty.loadPlayers();
            return activeParty;
        }
        return null;
    }

    /**
     * Returns <code>true</code> if a user is able to successfully join a Party.
     *
     * @param partyId the party's index number in the database.
     * @return <code>true</code> if the user is able to successfully join a Party; <code>false</code> otherwise.
     */

    public boolean joinParty(int partyId){
        boolean refreshOK = Admin.refreshParties();
        if(partyId >= 0 && refreshOK){
            int index = Admin.getIndexWithPartyId(partyId);
            if(Database.addPlayerToParty(getUserName(), partyId)){
                activeParty = Admin.getParties().get(index);
                activeParty.loadPlayers();
                return true;
            }
        }
        return false;
    }

    /**
     * This method is called when a player leaves a party
     */
    public synchronized static void leaveParty(){

        int playNumber = activeParty.getPlayerNumberOfUser();
        activeParty.stopInfoPuller();



        if (activeParty.getGame() != null){
            activeParty.getGame().quitGame();
        }

        if(playNumber == 0){
            Database.deleteParty(activeParty.getParty_id());
        }else{
            Database.leaveParty(username);
        }




        activeParty = null;

    }

    /**
     * This method is called when a player deletes a party
     */
    public static void deleteParty(){
        if(activeParty != null){
            activeParty.stopInfoPuller();
        }
        Database.deleteParty(activeParty.getParty_id());
        activeParty = null;
    }


    public String toString(){
        return "username: " + username + " displayname: " + displayname + " games played: " + numberOfGamesPlayed + "number of wins " + numberOfWins + " number of europeans: " + numberOfEuropeans + " active party: " + activeParty.toString();
    }
}
