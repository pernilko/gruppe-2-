import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    Stage window;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene loginWindow = new Scene(root);

        loginWindow.getStylesheets().add("style.css");
        window.setScene(loginWindow);
        window.show();
        window.setOnCloseRequest(event -> {

            event.consume();
            try {
            if (User.getActiveParty() != null) {

                    if (User.getActiveParty().getGame() != null) {
                        User.getActiveParty().getGame().quitGame();
                    }
                    User.leaveParty();

            }
        } catch (Exception f) {
            f.printStackTrace();
        }
            try {
                Database.disconnect();
            }catch (Exception e){
                e.printStackTrace();
            }

            // to make sure the system exits
            System.exit(0);
        });
    }
}












