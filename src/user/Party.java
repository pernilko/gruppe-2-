import javafx.beans.property.SimpleStringProperty;
import java.util.ArrayList;

/**
 * Stores all the relevant info about a party Contains methods to change the party variables and create an game object,
 * both locally and in a database
 *
 * @see Game
 * @see Database
 * @author Team 2
 */

public class Party {
    private ArrayList<Player> players;
    private Game game;
    public SimpleStringProperty partyName;
    private int totalRounds = 2;
    private int party_id;
    private PartyThread partyThread;



    public Party(String partyName, int party_id) {
        this.partyName = new SimpleStringProperty(partyName);
        this.party_id = party_id;
        this.players = new ArrayList<>();
    }

    public String getPartyName(){
        return partyName.get();
    }

    public int getParty_id(){
        return party_id;
    }

    public Game getGame(){
        return game;
    }

    public int getNumberOfPlayers() {
        return players.size();
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public int getTotalRounds() {
        return totalRounds;
    }

    /**
     * Find the corresponding player number to the user object logged in by comparing username
     *
     * @return the player number in this party object that has the same username as the user, returns -1 if not found
     */
    public int getPlayerNumberOfUser(){
        for(int i = 0; i< players.size(); i++){
            if(User.getUserName().equals(players.get(i).getUsername())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Sets the totalRounds both in the party object and in the database.
     *
     * @param totalRounds an int value that will be set as the total rounds
     */
    public void setTotalRounds(int totalRounds) {
        this.totalRounds = totalRounds;
        Database.changeTotalRounds(totalRounds, party_id);
    }

    /**
     * Updates the totalRounds variable in the party object by retrieving the info from the database
     *
     * @return number of rounds the party is set to play in the database
     */

    public int updateTotalRounds(){
        int totalRoundsRead = Database.getTotalRounds(party_id);
        totalRounds = totalRoundsRead;
        return totalRounds;
    }

    /**
     * Creates a game and adds all the necessary info. Gives the game object in the party an unique game id
     * based on the games already stored in the database. Sets the player state to true,
     * signalizing to the other players that this player is ready to start through the database. Creates a
     * game object containing total rounds that is wanted to play, the players, the game id,
     * the player number and the party id Starts running the game thread.
     *
     * @param playerNumb a unique int number in the party representing the player in the code and the database
     * @return a game object
     */
    public Game createGame(int playerNumb) {

        if(game == null) {
            int game_id;

            if(playerNumb == 0){
                game_id = Database.createGame(party_id);
                Database.setPlayerState(true,party_id, 0);
            }else{
                game_id = Database.getGameId(party_id);
            }

            game = new Game(totalRounds, players, game_id, playerNumb, party_id);

            game.startGame();

            return game;
        }
        return game;
    }

    /**
     * Updates the arrayList of players in the party if the new arrayList is not equal to the current.
     * @param newPlayers an arrayList of Player objects
     */
    public void updatePlayers(ArrayList<Player> newPlayers){
        if(newPlayers.size() != players.size()){
            players = newPlayers;
        }
    }

    /**
     * Loads the players in the party from the Database and adds the updated version
     */
    public void loadPlayers(){
        players = Database.getPlayersInParty(party_id);
    }

    /**
     * Creates and starts a new thread that updates the information about the party
     */
    public void startInfoPuller(){
        partyThread = new PartyThread(this);
        partyThread.start();
    }

    /**
     * Stops the info puller thread from running.
     *
     * {@link PartyThread#stopThread()}
     */
    public void stopInfoPuller(){
        PartyThread.stopThread();
        try {
            partyThread.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public boolean isGameCreated(){
        if(game == null){
            return false;
        }
        return true;
    }

    /**
     * Returns <code>true</code> if the party thread is still running. Means that teh program is still pulling
     * information from the database.
     *
     * @return <code>true</code> if the party thread is still running; <code>false</code> otherwise.
     */
    public boolean isInfoPullerAlive(){
        return partyThread.isAlive();
    }

    public String toString() {
        return party_id + ":" + partyName + ", " + totalRounds + " number of players: " + players.size();
    }
}
