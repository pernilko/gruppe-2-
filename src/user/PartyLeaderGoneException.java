/**
 * Thrown exception if there is something wrong with the set party leader object
 */

public class PartyLeaderGoneException extends RuntimeException {
    public PartyLeaderGoneException(){
        super("Party leader has quit game");
    }
}
