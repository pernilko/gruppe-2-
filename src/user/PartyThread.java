/**
 * Creates a thread that pulls information about who has joined and the number of rounds in the party.
 * Updates the local party object it is bound to
 */
public class PartyThread extends  Thread {
    private Party party;
    private static boolean forceQuit;
    private static boolean readyButton = false;
    public static boolean getReadyButton(){return  readyButton;}

    public PartyThread(Party party){
        this.party = party;
    }

    @Override
    public void run(){

        forceQuit = false;
        readyButton = false;

        if (party.getPlayerNumberOfUser() != 0) {

            while (!Database.isGameStarted(party.getParty_id()) && !forceQuit) {
                party.updatePlayers(Database.getPlayersInParty(party.getParty_id()));

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (!forceQuit) {
                party.createGame(party.getPlayerNumberOfUser());
            }
        } else {

            this.startWaitForEveryoneToConfirmLoop();

            while (!Database.isGameStarted(party.getParty_id()) && !forceQuit) {
                if (party.getNumberOfPlayers() >= 3) {
                    readyButton = true;
                }
                party.updatePlayers(Database.getPlayersInParty(party.getParty_id()));

                if (!Database.getReadyExceptForPartyLeader(party.getParty_id())) {
                    this.startWaitForEveryoneToConfirmLoop();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (!forceQuit) {
                Database.setPlayerState(true, party.getParty_id(), 0);
            }
        }
    }

    private void startWaitForEveryoneToConfirmLoop(){
        while (!Database.getReadyExceptForPartyLeader(party.getParty_id()) && !forceQuit) {
            party.updatePlayers(Database.getPlayersInParty(party.getParty_id()));
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is only to be called if someone exits the party page. Stops the thread.
     */
    public  static void stopThread(){
        forceQuit = true;
    }
}
