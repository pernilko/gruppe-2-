import java.util.ArrayList;

/**
 * Administrates users and parties in the game. There are no constructor in this class as it is only for keeping track
 * of the users and parties created in the game.
 *
 * @author Maria Lande
 * @version 1.0
 */
public class Admin {
    private static ArrayList<Party> parties = new ArrayList<>();
    private static User loginUser = null;

    /**
     * Returns a logged in user
     *
     * @return the user object representing the logged in user.
     */
    public static User getLoginUser(){
        return loginUser;
    }

    /**
     * Returns <code>true</code> if username and password is validated.
     *
     * @param username The users username.
     * @param password The users password.
     * @return <code>true</code> if password and username is validated; <code>false</code> otherwise.
     */
    public static boolean login(String username, String password){

        boolean passwordOK = HashWizard.validatePassword(password, Database.getPasswordHash(username));

        if(passwordOK){
            loginUser = new User(username, Database.getDisplayname(username));
            return true;
        }
        return false;
    }

    /**
     * Returns <code>true</code> if a completely new user is created
     *
     * @param username A String containing the chosen username
     * @param displayName A String containing the chosen display name
     * @param password A String containing the chosen password
     * @return <code>true</code> if the process of creating a new user is successful; <code>false</code> otherwise
     *
     */
    public static boolean createUser(String username, String displayName, String password){
        try{
            if(!(validateUsername(username))) {
                throw new Exception("Username must be between 1-20 letters");
            }
            if(!(validateDisplayname(displayName))){
                throw new Exception("Display name must be between 1-20 letters");
            }
            if(!(validatePassword(password))){
                throw new Exception("Password must be between 6-35 letters");
            }

            String passwordHash = HashWizard.generateHash(password);

            if(Database.createNewUser(username, displayName, passwordHash)){
                loginUser = new User(username, displayName);
                return true;
            }
            return false;
        }catch (Exception e){
            e.getMessage();
            return false;
        }
    }

    /**
     * Returns <code>true</code> if the passwords match.
     *
     * @param password the password
     * @param passwordRepeated the password repeated
     * @return <code>true</code> if the two passwords match; <code>false</code> otherwise.
     */
    public static boolean samePassword(String password, String passwordRepeated){
        if(!(password.equals(passwordRepeated))){
            return false;
        }
        return true;
    }

    /**
     * Returns the parties stored in an array administrated by Admin. Contains all the parties created.
     *
     * @return the parties in the array.
     */
    public static ArrayList<Party> getParties(){
        return parties;
    }

    /**
     * Returns <code>true</code> if the array containing parties is refreshed with new data from the database
     *
     * @return <code>true</code> if the parties in the array is successfully refreshed; <code>false</code>
     * otherwise
     */
    public static boolean refreshParties(){
        parties = Database.getParties();
        return true;
    }

    /**
     * Return the index number of a party in Admins array-list, based on the index in the database.
     *
     * @param partyIndex the index of the party in the database.
     * @return the index of the party in admins array-list that matches the index in the database.
     * If it is not in the list, method returns -1.
     */
    public static int getIndexWithPartyId(int partyIndex){
        int index = -1;
        for(int i = 0; i < parties.size(); i++){
            if(parties.get(i).getParty_id() == partyIndex){
                index = i;
            }
        }
        return index;
    }


    /**
     * Returns <code>true</code> if the length of the username is right.
     * Used in {@link Admin#createUser(String, String, String)}
     *
     * @param username the username created by createUser-method
     * @return <code>true</code> if the length is ok.
     */
    private static boolean validateUsername(String username){
        if(username == null || username.length() > 20){
            return false;
        }
        return true;
    }

    /**
     * Returns <code>true</code> if the length of the username is right.
     * Used in {@link Admin#createUser(String, String, String)}
     *
     * @param displayname the display name created by createUser-method
     * @return <code>true</code> if the length is ok.
     */
    private static boolean validateDisplayname(String displayname){
        if(displayname == null || displayname.length() > 20){
            return false;
        }
        return true;
    }

    /**
     * Returns <code>true</code> if the length of the username is right.
     * Used in {@link Admin#createUser(String, String, String)}
     *
     * @param password the password created by createUser-method
     * @return <code>true</code> if the length is ok.
     */
    private static boolean validatePassword(String password){
        if(password == null || password.length() < 6 || password.length() > 35){
            return false;
        }
        return true;
    }
}

